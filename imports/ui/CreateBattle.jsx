import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import TopBar from './TopBar'
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal} from "react-bootstrap";
import {Typeahead} from 'react-bootstrap-typeahead'; // ES2015


export default class AllBattles extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      allFighters : [],
      user:{},
      firstFighter : {
        surname: 'Первый Боец',
        id:''
      },
      secondFighter: {
        surname: 'Второй Боец',
        id:''
      },
      form:{
        first:'',
        second:''
      },
      tourId:'',
      surnames:[''],
    };
  }

logOut = () => {
  Meteor.logout()
  setTimeout(()=>this.props.history.push({pathname:'/'}),500)
}

  goTo = (path) => {
    this.props.history.push(path)
  }

  componentWillMount() { 
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
      else this.setState({user:result})
    })
    Meteor.call('getFightersBase', (err,result) => {
      if (err) alert(err)
      else{
        this.setState({allFighters:result})
        let surnames = []
        result.map((fighter,index) => {
          surnames.push({label:fighter.surname,id:fighter._id})
        })
        this.setState({surnames:surnames})
      }     
    })
    Meteor.call('findLastpairs', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)}     
    })
  }

  clickFirstFght = (fighter) => {
    if(fighter)
    this.setState({firstFighter: fighter})
  }

  clickSecondFght = (fighter) => {
    if(fighter)
    this.setState({secondFighter: fighter})
  }

  onChangeText = (e) => {
    newState = this.state.tourId
    newState = e.target.value
    this.setState({tourId: newState})
}

onChangeFighters = (e,number) => {
  newState = this.state.form
  newState[number] = e
  this.setState({form: newState})
}

  createBattle = () => {
    const tmp_fighters = []
    tmp_fighters[0] = this.state.firstFighter
    tmp_fighters[1] = this.state.secondFighter
    Meteor.call('insertOnePair',tmp_fighters,this.state.tourId,'Not Active', (err,result) => {
      if (err) alert(err)
      else{}     
    })
    Meteor.call('findLastpairs', (err,result) => {
      if (err) alert(err)
      else{}     
    })
    this.props.history.push({pathname:'all-battles'})
  }

  render(){
    return(
      <div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
          <div style={{marginLeft:50}}>
        <Panel>
          <Row>
						<Col xs={12}>
							<table width="100%" className="table table-striped table-bordered table-hover">
								<thead>
									<tr>
										<th>Боец 1</th>
										<th className="text-center">Боец 2</th>
										<th>Номер ковра</th>
									</tr>
								</thead>
								<tbody>
									<tr>
                    <td style={{width:170}}>
                      <Typeahead
                        onInputChange={(e)=>this.onChangeFighters(e,'first')}
                        onChange={(fighter)=>this.clickFirstFght(fighter[0])}
                        options={this.state.allFighters}
                        labelKey={option => `${option.surname} (${option.weightCategory})`}/>
                    </td>
                    <td style={{width:170}}>
                      <Typeahead
                        onInputChange={(e)=>this.onChangeFighters(e,'second')}
                        onChange={(fighter)=>this.clickSecondFght(fighter[0])}
                        options={this.state.allFighters}
                        labelKey={option => `${option.surname} (${option.weightCategory})`}/>
                    </td>
                    <td style={{width:200}}>
                      <div className="form-group">
                        <input
                          className="form-control"
                          placeholder="Номер ковра"
                          type="text"
                          autoFocus
                          onChange={(e)=>this.onChangeText(e)}
                          value={this.state.tourId} />
                      </div>
                    </td>
								  </tr>
								</tbody>
							</table>
						</Col>
					</Row>
				</Panel>
      </div>
      <div className="figtersTable-button-container">
        {
          this.state.firstFighter.surname!='Первый Боец' && this.state.secondFighter.surname!='Второй Боец' && this.state.tourId!=''
            ? <input type='button' value="Создать бой" onClick={()=>this.createBattle()} className="figtersTable-button"/>
            : null
        }
      </div>
    </div>
    )
  }
}