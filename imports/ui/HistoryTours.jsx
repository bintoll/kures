import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal } from "react-bootstrap";
import TopBar from './TopBar'


export default class HistoryTours extends Component  {
  constructor (props) {
		super(props);
		this.state = {
      dataTable:[],
      user:{},
    };
  	}

    componentWillMount() {
      Meteor.call('getUser',(err,result)=>{
        if(err) console.log(err)
          else this.setState({user:result})
      } )
      Meteor.call('getPairs', (err,result) => {
        if (err) alert(err)
        else{
          this.setState({dataTable:result})
        }     
      })
    }

    chunkArray = (arr, chunk,amountFighters) => {//devide all figters inti pairs
      let i, j, tmp = [];
      for (i = 0, j = parseInt(amountFighters); i < j; i += chunk) {
        tmp.push(arr.slice(i, i + chunk));
      }
      return tmp
    }
    callTour = (tour) => {
      Meteor.call('getPairs',(err,result) => {
        if (err) alert(err)
        else{
          console.log(result)
                }     
      })
      console.log(tour._id)
      this.props.history.push({pathname:'/tour',data:tour.data,amountFighters:tour.amountLevels,id:tour._id})      
    }

    callInfo = (tour) => {
      const pairs = this.chunkArray(tour.data[0],2,tour.amountLevels) 
      this.props.history.push({pathname:'/table',pairs:pairs,amountFighters:tour.amountLevels,data:tour.data,id:tour._id})      
    }

    callToss = (tour) => {
      const pairs = this.chunkArray(tour.data[0],2,tour.amountLevels) 
      this.props.history.push({pathname:'/toss',data:pairs})      
    }

    logOut = () => {
      Meteor.logout()
      setTimeout(()=>this.props.history.push({pathname:'/'}),500)
    }

    goTo = (path) => {
      this.props.history.push(path)
    }

  render(){
    return(
      <div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
        <div className="margin-from-topbar">
         <div className="figtersTable-pairs">
            <table width="60%" className="table table-striped table-bordered table-hover">
              <tbody>
                <tr>
                  <th>Дата Создания</th>
                  <th>Число Участников</th>
                  <th width="1%"></th>
                </tr>
                  { 
                    this.state.dataTable.map((item,index) => {
                    return(
                      <tr key={index}>
                        <td>{(item.createdAt).getDate()+ '.' + ((item.createdAt).getMonth()+1)+ ' '   +(item.createdAt).getHours()+':' +(item.createdAt).getMinutes()}</td>
                        <td>{item.amountLevels}</td>
                        <td className="text-center">
                                  <FormGroup>
                                    <DropdownButton
                                      id={'dropdown-users-' + item._id}
                                      bsSize="sm"
                                      bsStyle="link"
                                      title="Действие"
                                      pullRight
                                      disabled={false}>
                                      <MenuItem header>Выберите действие</MenuItem>
                                      <MenuItem divider />
                                      <MenuItem eventKey="1" onClick={()=>this.callTour(item)} >Показать сетку</MenuItem>
                                      <MenuItem eventKey="2" onClick={()=>this.callInfo(item)} >Показать информацию</MenuItem>
                                      <MenuItem eventKey="2" onClick={()=>this.callToss(item)} >Показать жеребьевку</MenuItem>
                                    </DropdownButton>
                                  </FormGroup>
                                </td>
                      </tr>)})
                  }
                </tbody>
            </table>
          </div>
        </div>
      </div>
    )
  }
}
