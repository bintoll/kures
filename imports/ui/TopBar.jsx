import React from "react";
import { browserHistory } from 'react-router'

const TriggerResize = () => {
	var height, topOffset, width;
	topOffset = 50;
	width = (this.window.innerWidth > 0 ? this.window.innerWidth : this.screen.width);
	if (width < 768) {
		$("div.navbar-collapse").addClass("collapse");
		topOffset = 100;
	} else {
		$("div.navbar-collapse").removeClass("collapse");
	}
	height = (this.window.innerHeight > 0 ? this.window.innerHeight : this.screen.height);
	height = height - topOffset;
	if (height < 1) {
		height = 1;
	}
	if (height > topOffset) {
		$("#page-wrapper").css("min-height", height + "px");
	}
}
const IndexHeaderStyles = {
	li: {
		cursor: 'pointer'
	}
};
class IndexHeader extends React.Component {
	componentDidMount () {
		TriggerResize();
		$(window).bind("resize", function () { TriggerResize() });
	}

  render() {
    console.log(this.props)
		const {currentUser} = this.props;
		const isAdmin = Roles.userIsInRole(currentUser._id, 'admin');
		const isUser = !Roles.userIsInRole(currentUser._id, 'admin');
		return (
			<nav role="navigation" style={{marginBottom: 0}} className="navbar navbar-default navbar-static-top">
				<div className="navbar-header">
					<button type="button" data-toggle="collapse" data-target=".navbar-collapse" className="navbar-toggle"><span className="sr-only">Toggle navigation</span><span className="icon-bar" />
						<span className="icon-bar" /><span className="icon-bar" />
					</button>
					<a onClick={() => this.props.goTo('/admin')} className="navbar-brand">Fight Managment</a>
				</div>
        <div>
				<ul className="nav navbar-top-links navbar-right">
					<li className="dropdown">{/*<a data-toggle="dropdown" className="dropdown-toggle"><i className="fa fa-user fa-fw" /><i className="fa fa-caret-down" /></a>*/}
						<ul className="dropdown-menu dropdown-user">
							<li>
								<a id="sign-out" onClick={() => alert('dasdsa')}><i className="fa fa-sign-in fa-fw" />dsadada</a>
							</li>
						</ul>
					</li>
				</ul>
				<div className="navbar-default sidebar" role="navigation">
					<div className="sidebar-nav navbar-collapse">
						<ul className="nav" id="side-menu">
            {
								isAdmin ? (
							<li style={IndexHeaderStyles.li}>
								<a
									className={null}
									onClick={() => this.props.goTo('/admin')}>
									<i className="fa fa-dashboard fa-fw" /> Панель управления
								</a>
							</li>
            ) : null
							}	
									<li style={IndexHeaderStyles.li}>
										<a
											className={null}
											onClick={()=>this.props.goTo('/table')}>
											<i className="fa fa-building fa-fw" /> Загрузить бойцов
										</a>
									</li>
                  <li style={[IndexHeaderStyles.li,{height:1000}]}>
										<a
											className={null}
											onClick={()=>this.props.goTo('/create-battle')}>
											<i className="fa fa-hand-rock-o fa-fw" /> Создать бой
										</a>
									</li>
                  <li style={[IndexHeaderStyles.li,{height:1000}]}>
										<a
											className={null}
											onClick={()=>this.props.goTo('/all-battles')}>
											<i className="fa fa-server fa-fw" /> Все бои
										</a>
									</li>
                  <li style={[IndexHeaderStyles.li,{height:1000}]}>
										<a
											className={null}
											onClick={()=>this.props.goTo('/connect-users')}>
											<i className="fa fa-user-circle fa-fw" /> Привязать пользователя
										</a>
									</li>
									<li style={[IndexHeaderStyles.li,{height:1000}]}>
										<a
											className={null}
											onClick={()=>this.props.goTo('/fighterBase')}>
											<i className="fa fa-users fa-fw" /> Все бойцы
										</a>
									</li>
                  <li style={[IndexHeaderStyles.li,{height:1000}]}>
										<a
											className={null}
											onClick={()=>this.props.logOut()}>
											<i className="fa fa-sign-out fa-fw" /> Выйти из системы
										</a>
									</li>							
						</ul>
					</div>
				</div>
        </div>
			</nav>
		);
	}
}
export default IndexHeader;