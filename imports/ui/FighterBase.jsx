import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal } from "react-bootstrap";
import TopBar from './TopBar'
import countries from '../constants/config'
import { withTracker } from 'meteor/react-meteor-data';


export default FighterBaseContainer = withTracker(() => {

const fightersBaseHandle = Meteor.subscribe('fightersBase');
const loading = !fightersBaseHandle.ready();
const fightersBase = FightersBase.find()
const fightersBaseExists = !loading && !!fightersBase;
console.log(!loading && fightersBase.fetch())
return {
  fightersBaseHandle,
  loading,
  fightersBase,
  fightersBaseExists
};
})(
class FighterBase extends Component  {
  constructor (props) {
		super(props);
		this.state = {
      dataTable:[],
      user:{},
      idFighter:'',
      form:{
        id:'',
        name:'',
        surname:'',
        country:'',
        weightCategory:''
      },
      newForm: {
        id:'',
        name:'',
        surname:'',
        country:'',
        weightCategory:''
      },
      showDeleteModal:false,
      createFighterModal:false,
    };
}
    
  componentWillMount() {
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
      else this.setState({user:result})
    })
    Meteor.call('getFightersBase', (err,result) => {
      if (err) alert(err)
      else{
        this.setState({dataTable:result})
      }     
    })
  }

  logOut = () => {
    Meteor.logout()
    setTimeout(()=>this.props.history.push({pathname:'/'}),500)
  }

  onChangeText = (e,fieldName) => {
    newState = this.state.form
    newState[fieldName] = e.target.value
    this.setState({form: newState})
  }
  
  onChangeTextNewFighter = (e,fieldName) => {
    newState = this.state.newForm
    newState[fieldName] = e.target.value
    this.setState({newForm: newState})
  }

  createFighter = () => {
    const {id,surname,name,country,weightCategory} = this.state.newForm
    Meteor.call('insertFighter',id,surname,name,country,weightCategory,(err,result)=>{
      if (err) alert(err)
      else {}  
    })
    this.setState({createFighterModal:false})
  }

  deleteFighter = (id) => {
    Meteor.call('removeFromBase',id,(err,result)=>{
      if (err) alert(err)
      else{}  
    })
    Meteor.call('getFightersBase', (err,result) => {
      if (err) alert(err)
      else{
          this.setState({dataTable:result})
      }     
    })
  }

  openModal = (fighter) => {
    newState = this.state.form
    newState.id = fighter.idProtocol
    newState.surname = fighter.surname
    newState.name = fighter.name
    newState.country = fighter.country
    newState.weightCategory = fighter.weightCategory
    this.setState({idFighter:fighter._id,form:newState,showDeleteModal:true})
  }

  changeFighter = () => {
    const {id,name,surname,country,weightCategory} = this.state.form
    Meteor.call('updateFightersBaseById',this.state.idFighter,id,name,surname,country,weightCategory,(err,result)=>{
      if (err) alert(err)
        else {}  
    })
    Meteor.call('getFightersBase', (err,result) => {
      if (err) alert(err)
      else{
        this.setState({dataTable:result})
      }     
    })
      this.setState({showDeleteModal:false})
  }

  goTo = (path) => {
    this.props.history.push(path)
  }
    

  render(){
    return(
      <div>
        <Modal show={this.state.showDeleteModal} onHide={() => this.setState({showDeleteModal: false})}>
					<Modal.Header closeButton>
						<Modal.Title>Редактирование данных</Modal.Title>
					</Modal.Header>
					<Modal.Body>
          <form role="form">
					<fieldset>
          <div className="form-group">
							<input
								className="form-control"
								placeholder={'Номер протокола'}
								type="text"
								autoFocus
                onChange={(e)=>this.onChangeText(e,'id')}
                value={this.state.form.id} />
						</div>
						<div className="form-group">
							<input
								className="form-control"
								placeholder={'Фамилия'}
								type="text"
								autoFocus
                onChange={(e)=>this.onChangeText(e,'surname')}
                value={this.state.form.surname} />
						</div>
						<div className="form-group">
							<input
								className="form-control"
			  				placeholder={'Имя'}
		  					type="text"
								onChange={(e)=>this.onChangeText(e,'name')}
								value={this.state.form.name} />
						</div>
            <div className="form-group">
							<input
								className="form-control"
			  				placeholder={'Страна'}
		  					type="text"
								onChange={(e)=>this.onChangeText(e,'country')}
								value={this.state.form.country} />
						</div>
            <div className="form-group">
							<input
								className="form-control"
			  				placeholder={'Весовая категория'}
		  					type="text"
								onChange={(e)=>this.onChangeText(e,'weightCategory')}
								value={this.state.form.weightCategory} />
						</div>
            </fieldset>
            </form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.setState({showDeleteModal: false})}>Отмена</Button>
						<Button onClick={()=>this.changeFighter()} bsStyle="success">Изменить</Button>
					</Modal.Footer>
				</Modal>
        <Modal show={this.state.createFighterModal} onHide={() => this.setState({createFighterModal: false})}>
					<Modal.Header closeButton>
						<Modal.Title>Создать бойца</Modal.Title>
					</Modal.Header>
					<Modal.Body>
          <form role="form">
					<fieldset>
          <div className="form-group">
							<input
								className="form-control"
								placeholder={"Номер протокола"}
								type="text"
								autoFocus
                onChange={(e)=>this.onChangeTextNewFighter(e,'id')}
                value={this.state.newForm.id} />
						</div>
						<div className="form-group">
							<input
								className="form-control"
								placeholder={"Фамилия"}
								type="text"
								autoFocus
                onChange={(e)=>this.onChangeTextNewFighter(e,'surname')}
                value={this.state.newForm.surname} />
						</div>
						<div className="form-group">
							<input
								className="form-control"
								placeholder={"Имя"}
		  					type="text"
								onChange={(e)=>this.onChangeTextNewFighter(e,'name')}
								value={this.state.newForm.name} />
						</div>
            <div className="form-group">
							<input
								className="form-control"
								placeholder={"Страна"}
		  					type="text"
								onChange={(e)=>this.onChangeTextNewFighter(e,'country')}
								value={this.state.newForm.country} />
						</div>
            <div className="form-group">
							<input
								className="form-control"
								placeholder={"Весовая категория"}
		  					type="text"
								onChange={(e)=>this.onChangeTextNewFighter(e,'weightCategory')}
								value={this.state.newForm.weightCategory} />
						</div>
            </fieldset>
            </form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.setState({createFighterModal: false})}>Отмена</Button>
						<Button onClick={()=>this.createFighter()} bsStyle="success">Создать</Button>
					</Modal.Footer>
				</Modal>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
        <div className="margin-from-topbar">
        <input type='button' value="Создать бойца" onClick={()=>this.setState({createFighterModal:true})} className="figtersTable-button"/>
          <Panel header="База данных бойцов">
          <Row>
            <Col xs={12}>
              <table width="100%" className="table table-striped table-bordered table-hover">
              <thead>
                        <tr>
                          <th>Номер протокола</th>
                          <th width="1%" className="text-center">Фамилия</th>
                          <th width="1%" className="text-center">Имя</th>
                          <th width="1%" className="text-center">Страна</th>
                          <th width="1%" className="text-center">Страна</th>
                          <th width="1%" className="text-center">Весовая категория</th>
                          <th width="1%"></th>
                        </tr>
                        </thead>
                <tbody>
                  {
                    this.state.dataTable.map((item,index) => {
                      const inputCountry = item.country
                      const countryKeys = Object.keys(countries)
                      const findCountryMatch = countryKeys.find(country => country.indexOf((inputCountry).toUpperCase()) != -1)
                      const countryFound = countries[findCountryMatch]
                      return(
                        <tr key={index}>
                          <td>{item.idProtocol}</td>
                          <td>{item.surname}</td>
                          <td>{item.name}</td>
                          <td>{item.country}</td>
                          <td>{countryFound}</td>
                          <td>{item.weightCategory}</td>
                          <td className="text-center">
                                  <FormGroup>
                                    <DropdownButton
                                      id={'dropdown-users-' + item._id}
                                      bsSize="sm"
                                      bsStyle="link"
                                      title="Действие"
                                      pullRight
                                      disabled={false}>
                                      <MenuItem header>Выберите действие</MenuItem>
                                      <MenuItem divider />
                                      <MenuItem eventKey="1" onClick={()=>this.deleteFighter(item._id)} >Удалить</MenuItem>
                                      <MenuItem eventKey="2" onClick={()=>this.openModal(item)} >Редактировать</MenuItem>
                                    </DropdownButton>
                                  </FormGroup>
                                </td>
                        </tr>)})
                  }
                </tbody>
              </table>
            </Col>
          </Row>
          </Panel>
        </div>
      </div>
    )
  }
})
