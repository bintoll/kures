import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

export default FightContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class Fight extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:300,
      pause:0,
      user:'',
      end:Date.now(),
      figters:[],
      isBattleStar: false,
      isBattleContinue:false,
      isBattleEnd:false,
      clearWin:false,
      isMedic:false,
      idTour:'',
      idOnePair:'',
      winner:'1000',
      pairs:[],
      score:[
        {
          buk:0,
          jambas:0,
          jenis:0,
          taza:0
        },
        {
          buk:0,
          jambas:0,
          jenis:0,
          taza:0
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.onePairExists && nextProps.onePair.fetch() && nextProps.onePair.fetch()!=this.props.onePair.fetch()){
      Meteor.call('findLastOnePairById',this.state.idOnePair, (err,result) => {
        if (err) alert(err)
        else{
          this.setState({figters:result.data,idTour:result.idTour,elapsed:result.battleTimer || 300,isMedic:result.isMedic || false,beltfight:result.beltfight || false}) 
          if(this.state.isMedic.active){
              let time = this.state.elapsed
              this.props.history.push({pathname:'/medic',data:this.state.isMedic.fighter,time:time,callback:this.state.figters,id:this.state.idTour,idOnePair:this.state.idOnePair})      
          }              
          if(this.state.beltfight.active){
              let time = this.state.elapsed
              this.props.history.push({pathname:'/beltfight',data:this.state.figters,time:time,callback:this.state.figters,id:this.state.idTour,idOnePair:this.state.idOnePair,userId:this.state.user})   
          }
          if(this.state.figters[0][3]>=1)
            this.setState({winner:this.state.figters[0]._id})
          if(this.state.figters[1][3]>=1)
            this.setState({winner:this.state.figters[1]._id})   
        }
      })
    }
  }

  componentWillMount() {
    let end = new Date() 
    end.setMinutes(end.getMinutes()+5)
    if(this.props.location.time){
      this.setState({isBattleStar:true})
      end = this.props.location.time
      this.setState({end: end,elapsed: end-Date.now()})}
      Meteor.call('findLastOnePairById',this.props.location.idOnePair, (err,result) => {
        if (err) alert(err)
        else{
          if(result.state=='Active')
          this.setState({isBattleStar:true,isBattleContinue:true})
          if(result.state=='Not Active')
            this.setState({isBattleStar:false,isBattleContinue:false})
          if(result.state=='Active-Paused')
            this.setState({isBattleStar:true,isBattleContinue:false})
        }
      })
    if(this.props.location.data){
      this.setState({figters:this.props.location.data,idTour:this.props.location.id,idOnePair:this.props.location.idOnePair})
      Meteor.call('findPairsById',this.props.location.id,(err,result) => {
        if (err) alert(err)
        else{
          this.setState({pairs:result})
          Meteor.call('findConnections',(err,result) => {
            if (err) alert(err)
            else{
              result.map((connection,index) => {
                if(connection.idTour==this.props.location.id)
                Meteor.call('updateUserAdmin',connection.user,'user-fight',this.state.figters,this.state.idTour,this.state.idOnePair,(err,result) => {              
                  if (err) alert(err)
                  else{
                    const state = this.props.location.state ?'Active-Paused' :'Active'
                    Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.elapsed,false,false,state,false,(err,result) => {
                      if (err) alert(err)
                      else{
                        Meteor.call('getAllConnections',(error, result) => {
                          if(error) alert(error)
                          else      console.log(result)
                          })
                        }     
                    })
                  }
                })
              })
            }     
          })
        }     
      })
    }
    else
      Meteor.call('findLastOnePair', (err,result) => {
        if (err) alert(err)
        else{
          this.setState({idOnePair:result._id,figters:result.data,idTour:result.idTour})
          Meteor.call('findPairsById',this.state.idTour,(err,result) => {
            if (err) alert(err)
            else{
              this.setState({pairs:result})
            }     
          })
          Meteor.call('getPairs',(err,result) => {
            if (err) alert(err)
            else{
            }     
          })
        }     
      })
  }
  

  tick = () => {
    Meteor.call('findLastOnePairById',this.state.idOnePair,(err,result) => {
      if (err) alert(err)
      else{
        this.setState({elapsed:result.battleTimer ?result.battleTimer :300})
      }     
    })
    if(this.state.elapsed<=0){
      clearInterval(this.timer);
      this.setState({isBattleEnd:true})
    }
  }

  stopTimer = () => { 
    Meteor.call('stopTimer',this.state.idOnePair,'Active-Paused',(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    this.setState({end:this.state.elapsed,isBattleContinue:false})
    console.log(this.state.elapsed)
    clearInterval(this.timer);
  }
  
  startTimer = (seconds) =>{
    this.setState({isBattleStar:true,isBattleContinue:true})//Disable button "START BATTLE"
    let end = new Date()
    end.setMinutes(end.getMinutes()+1 )
    this.setState({end:end})
    this.timer = setInterval(this.tick, 50);
    console.log(this.state.idOnePair)
    Meteor.call('startTimer',this.state.idTour,this.state.figters,this.state.idOnePair,'Active',(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })


  }

  continueTimer = () =>{
    Meteor.call('continueTimer',this.state.idOnePair,this.state.figters,'Active',(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    let end = Date.now()
    end = end +this.state.end
    this.setState({end:end,isBattleContinue:true})
    this.timer = setInterval(this.tick, 50);
  }

  callMedic = (fighter) => {
    const {isBattleStar,isBattleContinue,isBattleEnd} = this.state
    let time = this.state.elapsed
    const medic = {active:true,fighter:fighter}
    Meteor.call('callMedic',this.state.idOnePair,medic,fighter._id,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

  beltFight = () => {
    let time = this.state.elapsed
    this.stopTimer()
    const beltfight = {active:true,fighter:this.state.figters}
    Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.end - Date.now(),false,beltfight,'Active',false,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })     
  }

  endBattle = () => {
    this.stopTimer()
    Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.elapsed,false,false,'Finished',this.state.winner,(err,result) => {
      if (err) alert(err)
      else{}
    })
    Meteor.call('findConnections',(err,result) => {
      if (err) alert(err)
      else{
        result.map((connection,index) => {
          if(connection.idTour==this.state.idTour){
            Meteor.call('updateUserAdmin',connection.user,false,this.state.figters,this.state.idTour,this.state.idOnePair,(err,result) => {              
              if (err) alert(err)
              else{
                Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.elapsed,false,false,'Finished',this.state.winner,(err,result) => {
                  if (err) alert(err)
                  else{ }
                })
              }
            })
          }
        Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.elapsed,false,false,'Finished',this.state.winner,(err,result) => {
        if (err) alert(err)
        else{ 
          this.props.history.push({pathname:'/all-battles'})                                            
          }
        })
    })}})
  }

  winner = (fighter) => {
    this.setState({clearWin:true,winner:fighter._id})
    this.stopTimer()
    this.setState({isBattleEnd:true})
    Meteor.call('updateOnePairById',this.state.idOnePair,this.state.idTour,this.state.figters,this.state.elapsed,false,false,'Finished',fighter._id,(err,result) => {
      if (err) alert(err)
      else{}     
    })  
  }

  changeFighters = (fighter,newState,index) => {
    const newStateOnePair = this.state.figters
    newStateOnePair[index] = {...fighter}
    this.setState({score:newState,figters:newStateOnePair})
    Meteor.call('changeFighters',this.state.idOnePair,newStateOnePair,(err,result) => {
      if (err) alert(err)
      else{}     
    })
  }

  minus = (typeScore,fighter,index) => {
    const newState = this.state.score
    fighter[typeScore]
      ?fighter[typeScore] -= 1
      :fighter[typeScore] = 0
      console.log(fighter)
    switch(typeScore){
      case 0: {
        this.changeFighters(fighter,newState,index)
        break;
      }
      case 1: {
        this.changeFighters(fighter,newState,index)      
        break;
      }
      case 2: {
        this.changeFighters(fighter,newState,index)      
        break;
      }
      case 3: {
        break;
      }
    }
  }

  warning = (typeScore,fighter,index) => {
  fighter['warning' + typeScore]
  ?fighter['warning' + typeScore] = false 
  :fighter['warning' + typeScore] = 1
  this.changeFighters(fighter,newState,index)
  }

  scoring = (typeScore,fighter,index) => {
    const newState = this.state.score
    fighter[typeScore]
      ?fighter[typeScore] += 1
      :fighter[typeScore] = 1
    switch(typeScore){
      case 0: {
        this.changeFighters(fighter,newState,index)
        break;
      }
      case 1: {
        this.changeFighters(fighter,newState,index)      
        break;
      }
      case 2: {
        this.changeFighters(fighter,newState,index)      
        break;
      }
      case 3: {
        this.winner(fighter)
        break;
      }
    }
  }


  render(){
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1) ; 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = Math.round(+(seconds))
    return(
      <div>
        <div className='logo-icon'>
          <img onClick={()=>this.props.history.push('/all-battles')} src="logo.png" width="150px"/>
        </div>
        <div className='timer'>
          {
            this.state.isBattleEnd || seconds<=0
              ? <p>0:00</p>
              :<p>{seconds==300 ?5 :minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60}</p>
          }
          <div className='call-medic-button-container'>
            {
              this.state.isBattleEnd
                ? <div>
                  {
                    this.state.clearWin
                      ? <input type='button' disabled={false}  value="Закончить бой" onClick={()=>this.endBattle()} className="call-medic-button"/>
                      : <input type='button' disabled={false}  value="Режим поясной борьбы" onClick={()=>this.beltFight()} className="call-medic-button"/>

                  }
                  </div>
                : <div>
                 {
                    this.state.isBattleContinue
                      ? <input type='button' value="Остановить" disabled={false} onClick={()=>this.stopTimer()} className="call-medic-button"/>
                      : null
                  }
                  {
                    !this.state.isBattleStar
                      ? <input type='button' value="Начать бой" onClick={()=>this.startTimer(seconds)} className="call-medic-button"/>
                      : null
                  }
                  {
                    !this.state.isBattleContinue && this.state.isBattleStar
                      ? <input type='button' value="Продолжить" onClick={()=>this.continueTimer()} className="call-medic-button"/>
                      : null
                }
                </div>
            }
          </div>
        </div>
        {
          this.state.figters.map((fighter,index)=>{
            const inputCountry = fighter.country
            const countryKeys = Object.keys(countries)
            const findCountryMatch = countryKeys.find(country => country.indexOf((inputCountry).toUpperCase()) != -1)
            const countryFound = countries[findCountryMatch]
            return(
              <div key={index} className='fighter' style={{backgroundColor:this.state.winner == fighter._id ?"#20B2AA" :null,padding:30,borderStyle:this.state.winner == fighter._id ?'solid': null,borderWidth:5,borderColor:'black' }}>
                <img src={'/flags/' + countryFound.toLowerCase() + '.png'} width="100px" alt="флаг страны" className='country-flag'/>
              <div className="fighter-info">
                <p><b>{fighter.name}</b></p> 
                <p><b>{fighter.surname}</b></p>
                <p>{fighter.country}</p>
                <div>
                </div>
                </div>
                {
                  this.state.isBattleStar
                    ? <div style={{marginLeft:20}}>
                        <table width="60%" className="table table-striped table-bordered table-hover">
                            <thead>
                               <tr> 
                                 <th style={{backgroundColor:'white',width:'100%'}} colSpan="4"><input type='button' value="Объявить победителем" onClick={()=>this.winner(fighter)} className="call-medic" style={{width:'100%'}}/></th>
                              </tr>
>>>>>>> 1373404f13c599fa9ad3a0a5485424de64f88ef7
                              <tr>
                                <th style={{backgroundColor:'white',width:'100%'}} colSpan="4"><input type='button' value="Вызвать мед. помощь" onClick={()=>this.callMedic(fighter)} className="call-medic" style={{width:'100%'}}/></th>
                              </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <th style={{backgroundColor:'white'}}>Бук</th>
                                <th style={{backgroundColor:'white'}}>Жамбас</th>
                                <th style={{backgroundColor:'white'}}>Жартылай Женис</th>
                                <th style={{backgroundColor:'white'}}>Таза</th>
                              </tr>
                              <tr>
                                <td style={{backgroundColor:'white'}}><input type='button' value="+" onClick={()=>this.scoring(0,fighter,index)} className="call-medic" style={{width:'100%'}}/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="+" onClick={()=>this.scoring(1,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="+" onClick={()=>this.scoring(2,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="+" onClick={()=>this.scoring(3,fighter,index)} className="call-medic"/></td>
                                </tr>
                                <tr>
                                <td style={{backgroundColor:'white'}}><input type='button' value="-" onClick={()=>this.minus(0,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="-" onClick={()=>this.minus(1,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="-" onClick={()=>this.minus(2,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="-" onClick={()=>this.minus(3,fighter,index)} className="call-medic"/></td>
                                </tr>
                                <tr>
                                <td style={{backgroundColor:'white'}}><input type='button' value="П" onClick={()=>this.warning(0,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="П" onClick={()=>this.warning(1,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="П" onClick={()=>this.warning(2,fighter,index)} className="call-medic"/></td>
                                <td style={{backgroundColor:'white'}}><input type='button' value="П" onClick={()=>this.warning(3,fighter,index)} className="call-medic"/></td>
                                </tr>
                            </tbody>
                          </table>
                      </div>
                    : null
                }
                <div>
              </div>
              <table width="60%" className="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <th style={{backgroundColor:'white'}}>Бук</th>
                  <th style={{backgroundColor:'white'}}>Жамбас</th>
                  <th style={{backgroundColor:'white'}}>Жартылай Женис</th>
                  <th style={{backgroundColor:'white'}}>Таза</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                <td>{fighter[0] || 0}</td>
                  <td>{fighter[1] || 0}</td>
                  <td>{fighter[2] || 0}</td>
                  <td>{fighter[3] || 0}</td>
                </tr>
              </tbody>
            </table>
            </div>

            )
          })
        }
      </div>
    )
  }
})