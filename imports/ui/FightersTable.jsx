import React,{Component} from 'react'
import { BrowserRouter as Router, Route, Link, Redirect, withRouter, withHistory } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Random } from 'meteor/random'
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal } from "react-bootstrap";

import importDataFiles from '../public/importDataFiles.js'
import TopBar from './TopBar'


class FightersTable extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadTable:{},
      dataTable : [],
      pairs:[],
      tourData:[],
      showDeleteModal:false,
      amountFighters:'',
      canCreateTour:false,
      raspded:false,
      user:{},
      id:''
    };
}

componentWillMount() {
  Meteor.call('getUser',(err,result)=>{
    if(err) alert(err)
      else this.setState({user:result})
  })
  Meteor.call('getPairs', (err,result) => {
    if (err) alert(err)
    else{}     
  })
  if(this.props.location.pairs){
    this.setState({tourData:this.props.location.data,pairs:this.props.location.pairs, canCreateTour:true,amountFighters:this.props.location.amountFighters,id:this.props.location.id})
  }
  else
    this.setState({dataTable:[],pairs:[],tourData:[]})
}

  chunkArray = (arr, chunk) => {//devide all figters inti pairs
    let i, j, tmp = [];
    for (i = 1, j = parseInt(this.state.amountFighters); i < j; i += chunk) {
      tmp.push(arr.slice(i, i + chunk));
    }
    Meteor.call('insertPairsTour',tmp, (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    this.setState({pairs:tmp})
    let room = {}//one room
    let level = []//each level
    let rooms = []
    let data = parseInt(this.state.amountFighters)
    let amountsLevels = Math.log(data) / Math.log(2)//amount of levels = log(2)amountOfFighters
    for (let i=0,lvl=data;i<amountsLevels;i++,lvl=lvl/2){//create tournament table
      level=[]
      for(let j=0;j<lvl;j++){
        if(i==0){
          room={id:i + '.' + j,idProtocol:this.state.dataTable[j+1].idProtocol,name:this.state.dataTable[j+1].name,surname:this.state.dataTable[j+1].surname,country:this.state.dataTable[j+1].country,weightCategory:this.state.dataTable[j+1].weightCategory}
        }
        else{
          room={id:i + '.' + j}
        }
        level.push(room)
      }
      rooms.push(level)
      this.setState({tourData:rooms})
    }
    Meteor.call('insertPairs',rooms,data ,(err,result) => {
      if (err) alert(err)
      else{
        this.setState({id:result})
        console.log(result)
      }     
    })
    Meteor.call('getPairs', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    Meteor.call('findLastPair', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

 random = (min, max) => {
  var range = max - min + 1;
  return Math.floor(Math.random()*range) + min;
  }

  randomArray = (array) => {
    this.setState({showDeleteModal:false,canCreateTour:true,raspded:false})
    let arr = array
    let r_i; // случайный индекс
    let v; // временная переменная
    for (var i = 0; i < arr.length-1; i++) {
      /* получаем случайный индекс (кроме последнего) */
      r_i = this.random(1, arr.length-1);
      /* меняем местами случайный элемент массива с последним */
      v = arr[r_i];
      arr[r_i] = arr[arr.length-1];
      arr[arr.length-1] = v;
    }
    this.chunkArray(arr,2)//create pairs
    Meteor.call('insertFighters',this.state.dataTable, (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })

    Meteor.call('getFighters', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
}

logOut = () => {
  Meteor.logout()
  this.props.history.push({pathname:'/'})
}

updateFighterBase = () => {
  this.state.dataTable.map((fighter,index)=>{
    index>0 ? Meteor.call('updateFightersBase',fighter.idProtocol,fighter.name,fighter.surname,
              fighter.country,fighter.weightCategory, (err,result) => {
              if (err) alert(err)
              else{
                console.log(result)
              }     
            })
            : null
  })
  this.setState({canCreateTour:true})


}

parseExcel = () => {
  Meteor.call('excelToArray',this.state.uploadTable, (err,result) => {
    if (err) alert(err)
    else{
      this.setState({dataTable:result})  
      this.updateFighterBase()
    }     
  })
  this.setState({raspded:true})

}

createTour = () => {
  this.setState({canCreateTour:false})
  this.props.history.push({pathname:'/fighterBase'})
} 

goToToss = () => {//Toss - жеребьевка
  this.props.history.push({pathname:'/toss',data:this.state.pairs})
}

fileInput = (e) => {//upload excel file
  file = e.target.files[0]
  let uploadInstance = importDataFiles.insert({
    file: file,
    streams: 'dynamic',
    chunkSize: 'dynamic',
    allowWebWorkers: true
  }, false);

  this.setState({ inProgress: true });
  uploadInstance.on('start', () => { console.log('Starting'); });
  uploadInstance.on('progress', (progress, fileObj) => {
    console.log('Upload Percentage: ' + progress);
    console.log(uploadInstance.file)
  });
  uploadInstance.on('end', (error, fileObj) => {
    console.log('On end File Object: ', fileObj);
    console.log('ID: ', fileObj._id);
    console.log(fileObj)
    this.setState({ uploadTable: fileObj.path});
    this.parseExcel(fileObj._id) 
  });
  uploadInstance.start();     
}

goTo = (path) => {
  this.props.history.push(path)
}

  render(){
    console.log(this.state.dataTable)
    return(           
      <div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
        <div className="margin-from-topbar">
          <Modal show={this.state.showDeleteModal} onHide={() => this.setState({showDeleteModal: false})}>
            <Modal.Header closeButton>
              <Modal.Title>Создать турнир</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <FormControl componentClass="select" placeholder="Роли" bsSize="small" value={this.state.amountFigters} onChange={(e)=>this.setState({amountFighters:e.target.value})}>
                <option value="">-</option>
                <option value="8">8</option>
                <option value="16">16</option>
                <option value="32">32</option>
              </FormControl>
            </Modal.Body>
            <Modal.Footer>
              <Button onClick={() => this.setState({showDeleteModal: false})}>Отмена</Button>
              <Button onClick={()=>this.randomArray(this.state.dataTable)} bsStyle="success">Создать</Button>
            </Modal.Footer>
          </Modal>
          <input type='file' ref='inp' onChange={(e)=>this.fileInput(e)} className="figtersTable-button figtersTable-button-input"/>
            <div className="figtersTable-button-container">
              {
                this.state.canCreateTour
                  ? <input type='button' value="Внести в базу" onClick={()=>this.createTour()} className="figtersTable-button"/>
                  : null
              }
            </div>
          <Panel header="База данных бойцов">
            <Row>
              <Col xs={12}>
              {
                this.state.canCreateTour
                  ? null
                  : <table width="100%" className="table table-striped table-bordered table-hover">
                      <thead>
                            <tr>
                              <th>Номер протокола</th>
                              <th width="1%" className="text-center">Фамилия</th>
                              <th width="1%" className="text-center">Имя</th>
                              <th width="1%" className="text-center">Страна</th>
                              <th width="1%" className="text-center">Весовая категория</th>
                            </tr>
                            </thead>
                        <tbody>
                          {
                            this.state.dataTable.map((item,index) => {
                              return(
                                <tr key={index}>
                                  <td>{item.idProtocol}</td>
                                  <td>{item.surname}</td>
                                  <td>{item.name}</td>
                                  <td>{item.country}</td>
                                  <td>{item.weightCategory}</td>
                                </tr>)})
                          }
                        </tbody>
                      </table>   
              }
              </Col>
            </Row>
          </Panel>
        </div>
      </div>
    )
  }
}

export default FightersTable