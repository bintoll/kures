import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

import { withTracker } from 'meteor/react-meteor-data';

export default BeltFightContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class BeltFight extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:300,
      pause:0,
      end:Date.now(),
      fighters:[],
      isBattleStar: false,
      isBattleEnd:false,
      first_flag:'default',
      second_flag:'default',
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.onePairExists && nextProps.onePair.fetch() && nextProps.onePair.fetch()!=this.props.onePair.fetch()){
      Meteor.call('findLastOnePairById', this.props.location.idOnePair,(err,result) => {
        if (err) alert(err)
        else{
          if(result.beltfight.isStart) {//beltfight start battle
            this.startTime()
          }
          if(!result.beltfight.active) {//beltfight end
            this.props.history.push({pathname:'/user-fight'})  
          }
        }
      })
    }
  }

  componentWillMount() {
    let end = new Date() 
    end.setMinutes(end.getMinutes()+3)
    this.setState({end:end,elapsed:end - Date.now()})
    this.setState({fighters:this.props.location.data})
  }

  tick = () => {
    this.setState({elapsed: this.state.end - Date.now() });
    if(this.state.elapsed<=0)
      this.stopTimer()
  }

  stopTimer = () => { 
    clearInterval(this.timer);
  }

  startTime = () => {
    this.setState({isBattleStar:true})//Disable button "START BATTLE"
    let end = new Date()
    end.setMinutes(end.getMinutes()+3)
    this.setState({end:end})
    this.timer = setInterval(this.tick, 50);
    const beltfight={active:true,isStart:false}
    Meteor.call('updateOnePairById',this.props.location.idOnePair,this.props.location.id,this.props.location.data,false,false,beltfight,'Active',false,(err,result) => {
      if (err) alert(err)
      else{}
    })
  }
  
  startTimer = (seconds) =>{
    const beltfight={active:true,isStart:true}
    Meteor.call('updateOnePairById',this.props.location.idOnePair,this.props.location.id,this.props.location.data,false,false,beltfight,'Active',false,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

  render(){
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1) ; 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = Math.round(+(seconds))
    // this block find 3 letters of country (firstCountryFound, secondCountryFound)
    const firstInputCountry = this.state.fighters[0].country
    const secondInputCountry = this.state.fighters[1].country
    const countryKeys = Object.keys(countries)
    const firstFindCountryMatch = countryKeys.find(country => country.indexOf((firstInputCountry).toUpperCase()) != -1)
    const secondFindCountryMatch = countryKeys.find(country => country.indexOf((secondInputCountry).toUpperCase()) != -1)
    const firstCountryFound = countries[firstFindCountryMatch]
    const secondCountryFound = countries[secondFindCountryMatch]
    // this block find 3 letters of country (firstCountryFound, secondCountryFound)
    return(
      <div className="additional">
        <div className="content">
		      <div className="additionalTopBox">
			      <div className="tableRow">
				      <div className="cellLeft" style={{color:'white'}}>
                {minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60}
				      </div>
				      <div className="cellRight">
					      <img width="70%" src="logoWhite.png" />
				      </div>
			      </div>
		      </div>
		      <div className="additionalBottomBox">
			      <div className="tableRow">
				      <div className="cell" style={{color:'white'}}>
                <img src={'/flags/' + firstCountryFound.toLowerCase() + '.png'} width="60%" alt="флаг страны" />
                <p>{this.state.fighters[0].surname + ' ' + this.state.fighters[0].name.charAt(0) + '. '}<br /></p>
					      <span>{firstCountryFound  }</span>	
				      </div>
				      <div className="cell" style={{color:'white'}}>
                <img src={'/flags/' + secondCountryFound.toLowerCase() + '.png'} width="60%" alt="флаг страны" />
                <p>{this.state.fighters[1].surname + ' ' + this.state.fighters[1].name.charAt(0) + '. '}<br /></p>
					      <span>{secondCountryFound  }</span>						
				      </div>				
			      </div>
		      </div>
          <script src="modernizr-3.5.0.min.js"></script>
	      </div>  
      </div>
    )
  }
})