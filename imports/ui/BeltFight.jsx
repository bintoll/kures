import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

import { withTracker } from 'meteor/react-meteor-data';

export default BeltFightContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class BeltFight extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:300,
      pause:0,
      end:Date.now(),
      fighters:[],
      isBattleStar: false,
      isBattleEnd:false,
      winner:false,
    };
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.onePairExists && nextProps.onePair.fetch()){
      Meteor.call('findLastOnePairById', this.props.location.idOnePair,(err,result) => {
        if (err) alert(err)
        else{
          if(result.beltfight.isStart) {
            this.startTime()
          }
          if(!result.beltfight) {
            Meteor.call('findConnections',(err,result) => {
              if (err) alert(err)
              else {
                result.map((connection,index) => {
                  if(connection.idTour==this.state.idTour)
                    Meteor.call('updateUserAdmin',connection.user,false,this.state.fighters,this.state.idTour,this.state.idOnePair,(err,result) => {              
                      if (err) alert(err)
                      else{
                      }})
                    })}})
            this.props.history.push({pathname:'/all-battles'})                    
          }
        }
      })
    }
  }

  componentWillMount() {
    let end = new Date() 
    end.setMinutes(end.getMinutes()+3)
    this.setState({end:end,elapsed:end - Date.now()})
    this.setState({fighters:this.props.location.data,idTour:this.props.location.id})
  }

  tick = () => {
    this.setState({elapsed: this.state.end - Date.now() });
    if(this.state.elapsed<=0)
      this.stopTimer()
  }

  stopTimer = () => { 
    clearInterval(this.timer);
  }

  startTime = () => {
    this.setState({isBattleStar:true})//Disable button "START BATTLE"
    let end = new Date()
    end.setMinutes(end.getMinutes()+3)
    this.setState({end:end})
    this.timer = setInterval(this.tick, 50);

  }
  
  startTimer = () =>{
    const beltfight={active:true,isStart:true}
    Meteor.call('updateOnePairById',this.props.location.idOnePair,this.props.location.id,this.props.location.data,false,false,beltfight,'Active',false,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

  winner = (fighter) => {
    this.stopTimer()
    this.setState({winner:fighter._id})
    const beltfight = false
    Meteor.call('updateOnePairById',this.props.location.idOnePair,this.props.location.id,this.props.location.data,false,false,beltfight,'Finished',fighter._id,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

  render(){
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1) ; 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = Math.round(+(seconds))
    return(
      <div>
        <div className='logo-icon'>
          <img src="logo.png" width="200px"/>
        </div>
        <div className='timer'>
          <p>{minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60}</p>
          <div className='call-medic-button-container'>
            {
              !this.state.isBattleStar
                ? <input type='button' value="Начать бой" onClick={()=>this.startTimer()} className="call-medic-button"/>
                : null
            }
          </div>
        </div>
        <div className="belt-fighters">
        {
          this.state.fighters.map((fighter,index)=>{
            const inputCountry = fighter.country
            const countryKeys = Object.keys(countries)
            const findCountryMatch = countryKeys.find(country => country.indexOf((inputCountry).toUpperCase()) != -1)
            const countryFound = countries[findCountryMatch]
            return(
              <div key={index} className='belt-fighter'>
                <img src={'/flags/' + countryFound.toLowerCase() + '.png'} width="100px" alt="флаг страны" className='country-flag'/>
              <div className="fighter-info">
                <p><b>{fighter.name}</b></p> 
                <p><b>{fighter.surname}</b></p>
                <p>{fighter.country}</p>
                <div>
                <input type='button' value="Объявить победителя" onClick={()=>this.winner(fighter)} className="call-medic"/>
                </div>
              </div>
            </div>
            )
          })
        }
        </div>
      </div>
    )
  }
})