import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import TopBar from './TopBar'
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal} from "react-bootstrap";
import { withTracker } from 'meteor/react-meteor-data';

export default AllBattlesContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class CreateBattle extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      allPairs:[],
      user:{},
    };
  }

  logOut = () => {//TOP BAR Method
    Meteor.logout()
    setTimeout(()=>this.props.history.push({pathname:'/'}),500)
  }

  goTo = (path) => {//TOP BAR Method
    this.props.history.push(path)
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.onePairExists && nextProps.onePair.fetch() && nextProps.onePair.fetch()!=this.props.onePair.fetch()){
    this.setState({allPairs:nextProps.onePair.fetch()})
    }      
  }

  componentWillMount() { 
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
        else this.setState({user:result})
    })
    Meteor.call('findLastpairs', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
        this.setState({allPairs:result})
      }     
    })
  }

  startBattle = (pair) => {
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
        else {
          Roles.userIsInRole(result,['admin','judje'])
            ?this.props.history.push({pathname:'/fight',idOnePair:pair._id,data:pair.data,id:pair.idTour,time:pair.battleTimer + Date.now()})  
            :this.props.history.push({pathname:'/user-fight',data:pair,id:pair.idTour})  
        }
      })
  }

  continueBattle = (pair) => {
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
        else {
          Roles.userIsInRole(result,['admin','judje'])
            ?this.props.history.push({pathname:'/fight',idOnePair:pair._id,data:pair.data,id:pair.idTour,time:pair.battleTimer + Date.now()})  
            :this.props.history.push({pathname:'/user-fight',data:pair,id:pair.idTour})  
        }
      })
  }

  deleteOnePair = (idPair,idTour,fighters) => {
    Meteor.call('getAllConnections', (err,result)=>{
      if(err) console.log(err)
      else {
       console.log(result)
        result.map((connection,index) => {
          if(connection.idTour == idTour) {
            Meteor.call('updateUserAdmin',connection.user,false,connection.data,connection.idTour,false,(err,result) => {              
              if(err) console.log(err)
              else {
                Meteor.call('removeOnePair', idPair, (err,result)=>{
                  if(err) console.log(err)
                  else console.log(result)
                })
              }
            })
          }
        })
        Meteor.call('removeOnePair', idPair, (err,result)=>{
          if(err) console.log(err)
          else console.log(result)
        })
      }
    })
  }

  render(){
    return(
      <div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
        <div className="margin-from-topbar">
          <Panel>
            <Row>
							<Col xs={12}>
								<table width="100%" className="table table-striped table-bordered table-hover">
									<thead>
										<tr>
											<th>Боец 1</th>
											<th className="text-center">Боец 2</th>
											<th>Номер турнира</th>
                      <th>Состояние</th>
                      {
                        Roles.userIsInRole(Meteor.user(),'admin') || Roles.userIsInRole(Meteor.user(),'judje')
                          ? <th>Действия</th>
                          : null
                      }
										</tr>
									</thead>
									<tbody>
                    {
                      this.state.allPairs.map((pair,index)=> {
                        return(
                          <tr key={index}> 
                            <td style={{borderBottomWidth:2,borderLeftWidth:0,borderRightWidth:0,backgroundColor:pair.data[0]._id==pair.winnerID ?'#74DF00' :null}}>{pair.data[0].surname}</td>
                            <td style={{backgroundColor:pair.data[1]._id==pair.winnerID ?'#74DF00' :null}}>{pair.data[1].surname}</td>
                            <td>{pair.idTour}</td>
                            <td>{pair.state}</td>
                            <td className="text-center">
														<FormGroup>
															<DropdownButton
																id={1}
																bsSize="sm"
																bsStyle="link"
																title="Действие"
																pullRight
																disabled={false}>
															<MenuItem header>Выберите действие</MenuItem>
															<MenuItem divider />
															<MenuItem eventKey="1" onClick={()=>this.deleteOnePair(pair._id,pair.idTour,pair.data)} >Удалить</MenuItem>
                              {
                                Roles.userIsInRole(Meteor.user(),'admin') || Roles.userIsInRole(Meteor.user(),'judje')
                                  ? pair.state == 'Not Active'
                                    ?  <MenuItem onClick={()=>this.startBattle(pair)}>Начать бой</MenuItem>
                                    : pair.state == 'Active' || pair.state=='Active-Paused'
                                      ? <MenuItem onClick={()=>this.startBattle(pair)}>Вернуть к бою</MenuItem>
                                      : null
                                  :null
                              }
															</DropdownButton>
														</FormGroup>
														</td>
                          </tr>
                        )
                      })
                    }
									</tbody>
								</table>
							</Col>
						</Row>
					</Panel>
        </div>
      </div>
    )
  }
})