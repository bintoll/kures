import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import Ring from './components/Ring.jsx'
import windowSize from 'react-window-size';
import { Template } from 'meteor/templating';
import HistoryTours from '../client/collection/historyTours'



class Tour extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      pairs:[],
      fighterInfo:'',
      padding:0,
      levels:0,
      data:[],
      idOnePair:'',
      amountFighters:0,
      id:''
    };
}

componentWillReceiveProps(nextProps){
  /*console.log(nextProps.tour.fetch())
  if(nextProps.tour)
    {
      Meteor.call('findLastHistoryTours', (err,result) => {
        if (err) alert(err)
        else{
          console.log(result)
          this.setState({data:result.data,amountFighters:result.amountLevels,id:result.id})
          this.createTour()
        }     
      })
    }*/
}

  componentWillMount() {
    if(this.props.location.data){
      console.log(this.props.location)
    const {amountFighters,data} = this.props.location
    this.setState({levels:Math.log(amountFighters)/Math.log(2)})//levels calculates as log(2)amountFigters
    let amount = 2 * this.props.location.amountFighters//
    let allPairs = []
    let pairRight = []//for right table
    let pairLeft = []//for left table
    let roomRight = []
    let roomLeft = []
    this.props.location.data.map((level,i)=>{
      roomLeft=[]
      roomRight=[]
      amount=amount/2
      level.map((fighter,number)=>{
        if(number<amount/2 && amount>1) roomLeft.push(fighter)
        if(number>=amount/2 && amount>1) roomRight.push(fighter)
      })
      pairLeft.push(roomLeft)
      pairRight.push(roomRight)
    })
    allPairs.push(pairLeft)
    allPairs.push(pairRight)
    console.log(this.props.location.id)
    this.setState({pairs:allPairs,data:this.props.location.data,id:this.props.location.id})   
    Meteor.call('insertHistoryTours',this.props.location.data,amountFighters,(err,result)=>{
      if (err) console.log(err)
        else console.log(result)
    })  
  }
  else {
    Meteor.call('findLastHistoryTours', (err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
        this.setState({data:result.data,amountFighters:result.amountLevels,id:result.id})
        this.createTour()
      }     
    })
  }
  }

  createTour = () => {
    this.setState({levels:Math.log(this.state.amountFighters)/Math.log(2)})//levels calculates as log(2)amountFigters
    let amount = 2 * this.state.amountFighters//
    let allPairs = []
    let pairRight = []//for right table
    let pairLeft = []//for left table
    let roomRight = []
    let roomLeft = []
    this.state.data.map((level,i)=>{
      roomLeft=[]
      roomRight=[]
      amount=amount/2
      level.map((fighter,number)=>{
        if(number<amount/2 && amount>1) roomLeft.push(fighter)
        if(number>=amount/2 && amount>1) roomRight.push(fighter)
      })
      pairLeft.push(roomLeft)
      pairRight.push(roomRight)
    })
    allPairs.push(pairLeft)
    allPairs.push(pairRight)
    this.setState({pairs:allPairs})     
  }

  onFighterClick = (info) => {//go to battle
    const {id,name} = info
    fighterInfo = id ?id : name
    let fighters = []
    this.state.data.map((x,i)=>{
      x.map((y,ind)=>{
        if(y.id==(fighterInfo.charAt(0)-1+'.'+fighterInfo.substring(2)*2) || y.id==fighterInfo.charAt(0)-1+'.'+(+(fighterInfo.substring(2)*2)+1))
          if(y.name)
          fighters.push(y)
      })
    })
    console.log(fighters)
    if(fighters.length==0)
      alert('Этот бой не активен')
    else{
    Meteor.call('insertOnePair',fighters,this.state.id,(err,result)=>{
      if(err) console.log(err)
        else {
          this.setState({idOnePair:result})
          Meteor.call('getUser',(err,result)=>{
            if(err) console.log(err)
              else {
                Roles.userIsInRole(result,['admin','judje'])
                  ?this.props.history.push({pathname:'/fight',idOnePair:this.state.idOnePair,data:fighters,id:this.state.id})  
                  :this.props.history.push({pathname:'/user-fight',data:fighters,id:this.state.id})  
              }
          })
        }
    })
  }
  }

  render(){
    console.log(this.props)
    var padding=0
    const height = this.props.windowHeight/Math.pow(2,this.state.levels)
    const width = this.props.windowWidth
    const winner = {id:this.state.levels + '.' + '0'}
    console.log(winner)
    return(
      <div className='row-tables'>
        <p>{this.state.fighterInfo}</p>
        <div className='rowing-winner' style={{marginLeft: width/2+40-50,marginTop:Math.pow(2,this.state.levels-1)*height-130}}>
          <div className='rasd'></div>
          <div className='rasds'></div>
        </div>
        <div className='round-winner' style={{marginLeft: width/2-50,marginTop:Math.pow(2,this.state.levels-1)*height-100}}>
          <div onClick={()=>this.onFighterClick(winner)} className="round">
          <p style={{width:100,height:40}}>---</p>
        </div>
          </div>
          {
            this.state.pairs.map((table,i) =>
              {
                return(
                  <div className={i==0 ?"tour-table" :"tour-table-2"} key={i} style={{width:width/2}}>
                    {
                      table.map((pair,index)=>{
                        padding=Math.pow(2,index)*height-25
                        return(
                          <div key={index} style={{paddingTop:padding, height:Math.pow(2,this.state.levels)*height}}>
                            {
                              pair.map((fighter,ind)=>{
                                return(
                                  <div key={index + 'fght' + ind}>
                                    <Ring height={10} padding={padding} level={this.state.levels} fighter={fighter} onFighterClick={(info)=>this.onFighterClick(info)}/>
                                      {
                                        ind%2==0 && index!=this.state.levels-1
                                          ? <div className='rowing' style={{marginTop:-padding-(2*height*Math.pow(2,index))}}>
                                              <div style={{height:2 *(Math.pow(2,index+1)*height)}} className='rasd'></div>
                                              <div style={{height:Math.pow(2,index+1)*height}} className='rasds'></div>
                                            </div>
                                          : null
                                      }
                                  </div>
                                )
                              })
                            }
                          </div>
                        )
                      })
                    }     
                  </div>
              )})}
           </div>
      )
    }
  
  }

export default windowSize(Tour)
