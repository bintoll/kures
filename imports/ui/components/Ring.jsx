import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

export default class Ring extends Component  {

  render(){
    return(
      <div onClick={()=>this.props.onFighterClick(this.props.fighter)} className="round" style={{marginBottom:2*this.props.padding}}>
          <p style={{width:100,height:40}}>{this.props.fighter.name ?this.props.fighter.name : '---'}</p>
        </div>
    )
  }
}
