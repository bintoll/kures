import { BrowserRouter as Router, Route, Switch, Link, Redirect, withRouter} from 'react-router-dom'
import React,{Component} from 'react'
import Login from './Login.jsx'
import AdminPanelContainer from './AdminPanel'
import FightersTable from './FightersTable'
import CallMedic from './CallMedic'
import FightContainer from './Fight'
import UserFightContainer from './UserFight'
import UserBeltFight from './UserBeltFight'
import UserCallMedic from './UserCallMedic'
import TopBar from './TopBar'
import ConnectUsers from './ConnectUsers'
import FighterBaseContainer from './FighterBase'
import BeltFight from './BeltFight'
import AllBattlesContainer from './AllBattles'
import CreateBattle from './CreateBattle'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import { withTracker } from 'meteor/react-meteor-data';

export default PageRouterContainer = withTracker(() => {
  
    const user = Meteor.subscribe('user');
    const tekUser = Meteor.user()
    const loading = !user.ready();
    const userExists = !loading && !!user;
    console.log(userExists)
    return {
      user,
      tekUser,
      loading,
      userExists
    };
  })(

class PageRouter extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      username: '',
      fighters:[],
      user:{},
      load:true
    };
}
  createTour = (fighters) => {
    console.log(fighters)
    this.setState({fighters:fighters})
  }

componentWillReceiveProps(nextProps)
{
  console.log(nextProps)
  if(nextProps.userExists)
  this.setState({user:nextProps.tekUser,load:false})
}
  
   PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
      this.Auth() ?         <Component {...props}/>
      : this.isUser()
      ?(<Redirect to={{pathname: '/table'}}/>)
      :(<Redirect to={{pathname: '/'}}/>)
    )}/>
  )

  LoginRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
      this.Auth() ? (<Redirect to={{pathname: '/admin'}}/>)
      : this.JudjeAuth() 
        ? (<Redirect to={{pathname: '/table'}}/>)
        : <Component {...props}/>
    )}/>
  )

  judjeRoute = ({ component: Component, ...rest }) => (
    <Route {...rest} render={props => (
      this.JudjeAuth() ?         <Component {...props}/>
             : this.isUser()
                ?(<Redirect to={{pathname: '/user-fight'}}/>)
                :(<Redirect to={{pathname: '/'}}/>)
    )}/>
  )

      Auth = () => {
    if(Roles.userIsInRole(Meteor.user(),'admin')) {
      console.log(this.state.user)
      console.log(Roles.userIsInRole(this.state.user,'admin'))
      return true}
    else{
        console.log(Roles.userIsInRole(this.state.user,'admin'))
        
        return false
      }
    }

    isUser = () => {
      if(Meteor.user()){
        return true
      }
      else
        {
          return false
        }
    }



    JudjeAuth = () => {
      if(Roles.userIsInRole(Meteor.user(),'judje') || Roles.userIsInRole(Meteor.user(),'admin')){
        console.log('JUDJE')
        return true}
      else{
          console.log(Roles.userIsInRole(this.state.user,'admin'))
          console.log('JUDJE')
          return false
        }
      }

      UserAuth = () => {
        if(Roles.userIsInRole(this.state.user,'judje') || Roles.userIsInRole(this.state.user,'admin')) {
          return true}
        else{
            console.log(Roles.userIsInRole(this.state.user,'admin'))
            return false
          }
        }

  test = () => {
<Redirect to={{
  pathname: '/tour',
}}/>  }


  render(){
    console.log(this)
    return(
    <div>
      {
        !this.state.load
        ?<Router>
          <Switch>
          <this.LoginRoute exact path="/" component={Login}/>
          <this.PrivateRoute path="/admin" component={AdminPanelContainer}/>  
          <this.judjeRoute path="/table" component={FightersTable}/>  
          <this.judjeRoute  path="/fight"  component={FightContainer}/> 
          <this.judjeRoute  path="/medic"  component={CallMedic}/> 
          <this.judjeRoute  path="/fighterBase"  component={FighterBaseContainer}/> 
          <this.judjeRoute  path="/connect-users"  component={ConnectUsers}/> 
          <this.judjeRoute  path="/beltfight"  component={BeltFight}/> 
          <Route path="/user-fight"  component={UserFightContainer}/> 
          <Route path="/user-medic"  component={UserCallMedic}/> 
          <this.judjeRoute path="/create-battle"  component={CreateBattle}/> 
          <this.judjeRoute path="/all-battles"  component={AllBattlesContainer}/> 
          <Route path="/user-beltfight"  component={UserBeltFight}/>
        </Switch>
      </Router>
      : <img src="loading.png" width="300px"/>
      }
    </div>
    )
  }

})