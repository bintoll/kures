import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

export default class Toss extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      pairs: []
    };
}

componentWillMount() {
  if(this.props.location.data)
    this.setState({pairs:this.props.location.data})
  else {
  Meteor.call('findLastPairTour',(err,result)=>{
    if(err) console.log(err)
      else
        this.setState({pairs:result.data})
  })  }
}




  render(){
    return(
      <div>
        <div className='logo-icon'>
          <img src="logo.png" width="200px"/>
          </div>
        <div className="toss">
          <p>Распределение бойцов</p>
        </div>
          { 
            this.state.pairs.map((pair,index) => {
              return(
                <div className="toss-pair" key={index}>
                  {
                    pair.map((fighter,ind) => {
                      return(
                        <div key={index + 'fgt' + ind} className='toss-fighter'>
                          {
                            fighter.country == 'Россия'
                              ? <img src="russia.jpg" alt="флаг россии" width='100px' className='country-flag'/>
                              : <img src="kazakhstan.jpg" alt="флаг казахстана" width='100px' className='country-flag'/>
                          }
                          <div className="fighter-info">
                            <p><b>{fighter.name}    {fighter.surname}</b></p> 
                            <p>{fighter.country}</p>
                          </div>
                        </div>
                      )
                    })
                  }
                </div>
              )
            })
          }
        </div>
    )
  }

}
