import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import { withTracker } from 'meteor/react-meteor-data';

export default CallMedicContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class CallMedic extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:150,
      end:Date.now(),
      name:'',
      surname:'',
      country:'',
      onePair:{},
      idOnePair:'',
      fighterId:'',
      medicTimer:0,
      
    };
}

  componentDidMount() {
    Meteor.call('findLastOnePairById',this.state.idOnePair, (err,result) => {
      if (err) alert(err)
        else {
          console.log(result)
          if(!result.medicTimer){ 
            Meteor.call('startMedicTimer',this.state.idOnePair,this.state.fighterId, (err,result) => {
              if (err) alert(err)
              else{}          
            })   
          }
          else {
            this.setState({elapsed:result.medicTimer})
            Meteor.call('continueMedicTimer',this.state.idOnePair,this.state.fighterId, (err,result) => {
              if (err) alert(err)
              else{}          
            }) 
        }}
    })
    let end = new Date()
    end.setMinutes(end.getMinutes()+3)
    this.setState({end:end})
  }

  componentWillReceiveProps(nextProps)
  {
    console.log('dasda')
    Meteor.call('findLastOnePairById',this.state.idOnePair, (err,result) => {
      if (err) alert(err)
      else{
        this.setState({figters:result.data,idTour:result.idTour,isMedic:result.isMedic || false,beltfight:result.beltfight || false,elapsed: result.medicTimer - Date.now() || false}) 
        console.log(result)
        if(!result.isMedic)
          {
            this.props.history.push({pathname:'/fight',time:this.props.location.time + Date.now(),data:this.state.onePair.data,id:this.state.onePair.idTour,idOnePair:this.props.location.idOnePair,state:true})              
          }   
        }
      })
  }

  componentWillMount(){
    console.log(this.props)
    if(this.props.location.data){
      console.log(this.props)
      const {name,surname,country} = this.props.location.data
      this.setState({name:name,surname:surname,country:country,idOnePair:this.props.location.idOnePair,fighterId:this.props.location.data._id})}
      else
        {
          Meteor.call('findLastOnePair', (err,result) => {
            if (err) alert(err)
            else{
              this.setState({name:result.isMedic.fighter.name,surname:result.isMedic.fighter.surname,country:result.isMedic.fighter.country}) 
              console.log(result)
            }
          })
        }
  }

  tick = () => {
    this.setState({elapsed: this.state.end - Date.now() });
    if(this.state.elapsed<=0)
      this.stopTimer()
  }

  stopTimer = () =>{ 
          Meteor.call('endMedic',this.state.idOnePair,false,this.state.fighterId,(err,result) => {
            if (err) alert(err)
            else{
              console.log(result)
            }     
          })
          Meteor.call('findLastOnePair', (err,result) => {
            if (err) alert(err)
            else{
              this.setState({onePair:result})
              console.log(result)
              const medic = false
              console.log(this.state.idOnePair)
        }
      })

    clearInterval(this.timer);
    console.log(this.props.location)    //this.props.history.push({pathname:'/fight',data:fighters,id:this.state.id})  
    
  }

  render(){
    //const name = "sam"
    //const surname = "sam"
   // const country = 'DASD'
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1) ; 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = Math.round(+(seconds))
    return(
      <div>
        <div className='logo-icon'>
          <img src="logo.png" width="180px"/>
        </div>
        <div className='timer'>
          <p>Медицинская помощь</p>
          {
            seconds<=0
              ? <p>0:00</p>
              :<p>{minutes==3 ?2 : minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60}</p>
          }          <div className='call-medic-button-container'>
            <input type='button' value="Остановить" onClick={()=>this.stopTimer()} className="call-medic-button"/>
          </div>
          <div className="call-med-img">
            <img src="callMedic.png" width="400px" className="call-med"/>
          </div>
          <div className='fighter'>
            <div className="call-medic-info" ref="myRef">
              <p><b>{this.state.name}  </b></p> 
              <p><b>{this.state.surname} </b></p>
              <p>({this.state.country})</p>
            </div>
          </div>
        </div>
      </div>
    )
  }
})
