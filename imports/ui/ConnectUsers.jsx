import React,{Component} from "react";
import { BrowserRouter as Router, Route, Redirect, withRouter, withHistory} from 'react-router-dom'
import {createContainer} from "meteor/react-meteor-data";
import TopBar from './TopBar'
import HeaderTitle from "../client/Index/HeaderTitle";
import * as constants from "../client/Index/constants";
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal} from "react-bootstrap";

import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

export default AdminPanelContainer = withTracker(() => {
  
    const users = Meteor.subscribe('user');
    const loading = !users.ready();
    return {
      users,
      loading,
    };
  })(
class AdminPanel extends Component {
	constructor (props) {
		super(props);
		this.state = {
			showDeleteModal: false,
			name: '',
      role: '',
      allAccounts:[],
      tourNumber:'',
      user:{},
      userId: '',
      connections: []
		};
	}
  
  componentWillMount(){
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
      else this.setState({user:result})
    })
    this.getAllAccounts()//get all accs in state.allAccounts
    this.getAllConnections()
  }

getAllConnections = () => {
  Meteor.call('getAllConnections',(error, result) => {
    if(error) alert(error)
    else      this.setState({connections: result})
  })
}

logOut = () => {
  Meteor.logout()
  setTimeout(()=>this.props.history.push({pathname:'/'}),500)
}

  createUser = () => {//insert user in db
    const user = this.state.form.createUser
    const password = this.state.form.password
    Meteor.call('createAccount',user,password, (error, result) => {
      if(error) alert(error)
      else      this.getAllAccounts()})
    this.setState({showDeleteModal:false})
}

  getAllAccounts = () => {//get all accs in state.allAccounts
    Meteor.call('getAllAccounts', (error, result) => {
      if(error)  console.log(error)
      else       this.setState({allAccounts: result})})
}

filterAccs = (role) => {//filer accounts in table by role
  this.setState({role:role})
  if(role=='admin')
    {
      let allAccounts = []
      this.state.allAccounts.map((user)=>{
        Roles.userIsInRole(user,'admin')
        ? allAccounts.push(user)
        : null
      })
      this.setState({allAccounts:allAccounts})
    }
    else {
     this.getAllAccounts() 
    }
}

findAcc = (username) => {//find acc by substring inserted in field
  let len = username.length
  let allAccounts = []
  this.setState({name:username})
  this.state.allAccounts.map((user)=>{
    user.username.substring(0,len) == username
    ? allAccounts.push(user)
    : null
  })
  this.setState({allAccounts:allAccounts})
  if(username=='')
    this.getAllAccounts()
}

  changeRole = (addOrRemove,username,role) => {//remove or delete role on user, addOrRemove = true if add role
    let userToRole
    this.getAllAccounts()
    this.state.allAccounts.map((user,index) => {
      user.username == username
        ? userToRole = user
        :null})
    addOrRemove
      ? Meteor.call('addRoleToUser', userToRole, role, (error, result) => {
          if(error)  alert(error)
          else       this.getAllAccounts()})
      : Meteor.call('removeRoleFromUser', userToRole, role, (error, result) => {
        if(error)  alert(error)
          else      this.getAllAccounts()})
}

  onChangeText = (e) => {
    newState = this.state.tourNumber
    newState = e.target.value
    this.setState({tourNumber: newState})
}

deleteAccount = (username) => {
  let userTodelete
  this.getAllAccounts()
  this.state.allAccounts.map((user,index) => {
    user.username == username
      ? userToRole = user
      :null })
    Meteor.call('deleteAccount', userToRole._id,(error, result) => {
      if(error) alert(error)
      else      this.getAllAccounts()})
}

goTo = (path) => {
  this.props.history.push(path)
}

connectUser = () => {
  this.setState({showDeleteModal:false})  
  Meteor.call('connectUser', this.state.userId,this.state.tourNumber,(error, result) => {
    if(error) alert(error)
    else this.getAllConnections()
  })
}


	render () {
		return (
			<div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
				<Modal show={this.state.showDeleteModal} onHide={() => this.setState({userId: null, showDeleteModal: false})}>
					<Modal.Header closeButton>
						<Modal.Title>Привязть пользователя к турниру</Modal.Title>
					</Modal.Header>
					<Modal.Body>
            <form role="form">
              <fieldset>
                <div className="form-group">
                  <input
                    className="form-control"
                    placeholder="Введите номер турнира"
                    type="text"
                    autoFocus
                    onChange={(e)=>this.onChangeText(e)}
                    value={this.state.tourNumber} />
                </div>
              </fieldset>
            </form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.setState({userId: null, showDeleteModal: false})}>Отмена</Button>
						<Button onClick={()=>this.connectUser()} bsStyle="success"> Привязать</Button>
					</Modal.Footer>
				</Modal>
        <div style={{marginLeft:260}}>
				<HeaderTitle title="Пользователи"/>
				<Row>
					<Col xs={12}>
						<Panel header="База данных пользователей">
							<Row>
								<Col xs={12}>
									<table width="100%" className="table table-striped table-bordered table-hover">
										<thead>
										<tr>
											<th>Имя</th>
											<th width="2%">Номер турнира</th>
											<th width="1%"></th>
										</tr>
										</thead>
										<tbody>
										{
											this.state.allAccounts.map((_user) => {
												return (
                          Roles.userIsInRole(_user,['admin','judje'])
                            ? null
                            : <tr key={'row-' + _user._id}>
                              <td>{_user.username}</td>
                              <td className="text-center">
                              {
                                this.state.connections && this.state.connections.find(user => user.user == _user._id) && this.state.connections.find(user => user.user == _user._id).idTour
                              }
                              </td>
														<td className="text-center">
															<FormGroup>
																<DropdownButton
																	id={'dropdown-users-' + _user._id}
																	bsSize="sm"
																	bsStyle="link"
																	title="Действие"
																	pullRight
																	disabled={false}>
																	<MenuItem header>Выберите действие</MenuItem>
																	<MenuItem divider />
                                  <MenuItem eventKey="2" onClick={()=>this.setState({showDeleteModal:true,userId:_user._id})}>Привязать пользователя</MenuItem>
																</DropdownButton>
															</FormGroup>
														</td>
													</tr> 
												);
											})
										}
										</tbody>
									</table>
								</Col>
							</Row>
						</Panel>
					</Col>
				</Row>
        </div>
      </div>
		);
	}
}
)
