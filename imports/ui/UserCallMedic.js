import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import { withTracker } from 'meteor/react-meteor-data';
import countries from '../constants/config'

export default CallMedicContainer = withTracker(() => {
  
    const onePairHandle = Meteor.subscribe('onePair');
    const loading = !onePairHandle.ready();
    const onePair = OnePair.find()
    const onePairExists = !loading && !!onePair;
    console.log(!loading && OnePair.find().fetch())
    return {
      onePairHandle,
      loading,
      onePair,
      onePairExists
    };
  })(
class CallMedic extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:150,
      end:Date.now(),
      name:'',
      surname:'',
      country:'',
      onePair:{},
      medicTimer:0,
    };
}

  componentDidMount() {
    Meteor.call('findLastOnePairById',this.props.location.idOnePair, (err,result) => {
      if (err) alert(err)
      else{
        this.setState({figters:result.data,idTour:result.idTour,elapsed: result.medicTimer - Date.now() || false}) 
        if(!result.isMedic){
            this.props.history.push({pathname:'/fight',time:this.props.location.time + Date.now(),data:this.state.onePair.data,id:this.state.onePair.idTour,idOnePair:this.props.location.idOnePair})              
          }   
        }
     })
  }

  componentWillReceiveProps(nextProps)
  {
    Meteor.call('findLastOnePairById',this.state.idOnePair, (err,result) => {
      if (err) alert(err)
      else{
        this.setState({figters:result.data,idTour:result.idTour,elapsed: result.medicTimer - Date.now() || false}) 
        if(!result.isMedic){
            this.props.history.push({pathname:'/user-fight',time:this.props.location.time + Date.now(),data:this.state.figters,id:this.state.onePair.idTour,idOnePair:this.props.location.idOnePair})              
          }   
        }
     })
  }

  componentWillMount(){
    if(this.props.location.data){
      const {name,surname,country} = this.props.location.data
      this.setState({name:name,surname:surname,country:country,idOnePair:this.props.location.idOnePair})}
      else{
          Meteor.call('findLastOnePair', (err,result) => {
            if (err) alert(err)
            else{
              this.setState({name:result.isMedic.fighter.name,surname:result.isMedic.fighter.surname,country:result.isMedic.fighter.country}) 
            }
          })
        }
  }

  tick = () => {
    this.setState({elapsed: this.state.end - Date.now() });
    if(this.state.elapsed<=0)
      this.stopTimer()
  }

  stopTimer = () =>{ 
      Meteor.call('findLastOnePair', (err,result) => {
        if (err) alert(err)
        else{
          this.setState({onePair:result})
          const medic = false
        Meteor.call('updateOnePair',this.state.onePair.idTour,this.state.onePair.data,this.state.onePair.battleTimer,medic,(err,result) => {
          if (err) alert(err)
          else{}     
        })
        }
      })
    clearInterval(this.timer);    
  }

  render(){
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1); 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = (Math.round(+(seconds)))
    const inputCountry = this.state.country
    const countryKeys = Object.keys(countries)
    const findCountryMatch = countryKeys.find(country => country.indexOf((inputCountry).toUpperCase()) != -1)
    const countryFound = countries[findCountryMatch]
    return(
      <div className="medical">
        <div className="content">
		      <div className="medicalTopBox">
			      <div className="tableRow">
				      <div className="cellLeft">
				    </div>
				    <div className="cellRight">
					    <img width="70%" src="logoWhite.png"/>
				    </div>
			    </div>
		    </div>
		    <div className="medicalBottomBox">
			    <div className="tableRow">
				    <div className="cell" style={{width: '40%'}}>
					    <img src="plus.png" width="40%"/>
				    </div>
				    <div className="cell" style={{width: '20%'}}>
					    <img src="whitepx.png" height="35%" width="2"/>
				    </div>
				    <div className="cell timerText" style={{width: '40%',color:'white'}}>
              {
                seconds<=0
                  ? <p>0:00</p>
                  : <p>{minutes==3 ?2 : minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60}</p>
              }
				    </div>
			    </div>
		    </div>
		    <div className="medicalBottomBox second">
			    <div className="tableRow">
				    <div className="cell bottom" style={{width: '100%',color:'white'}}>
              <img src={'/flags/' + countryFound.toLowerCase() + '.png'} width="15%" alt="флаг страны" />
					    <p>{this.state.surname + ' ' + this.state.name.charAt(0) + '. '}<br /></p>
				    </div>
			    </div>
		    </div>
        <script src="modernizr-3.5.0.min.js"></script>
	    </div>
    </div>
    )
  }
})