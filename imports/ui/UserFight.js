import React,{Component} from 'react'
import { Meteor } from 'meteor/meteor'
import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';
import '/client/custom.less' 
import countries from '../constants/config'


export default UserFightContainer = withTracker(() => {
  
    const userAdminHandle = Meteor.subscribe('userAdmin');
    const onePairHandle = Meteor.subscribe('onePair');
    const onePair = OnePair.find()
    const userAdmin = UserAdmin.find()
    const loading = !onePairHandle.ready() && !userAdminHandle.ready();
    const onePairExists = !loading && !!onePair;
    const userAdminExists = !loading && !!userAdmin
    console.log(!loading && OnePair.find().fetch())
    console.log(onePairExists)
    return {
      onePairHandle,
      userAdminHandle,
      loading,
      onePair,
      userAdmin,
      onePairHandle,
      userAdminHandle,
      onePairExists,
      userAdminExists,
    };
  })(
class Fight extends Component  {
  constructor(props) {
    super(props);
    this.state = {
      elapsed:10,
      pause:0,
      end:Date.now(),
      figters:[
        {
          country:''
        },
        {
          country:''
        },
      ],
      isBattleStar: false,
      isBattleContinue:false,
      isBattleEnd:false,
      clearWin:false,
      isMedic:false,
      idTour:'',
      active:false,
      pairs:[],
      first_flag:'default',
      second_flag:'default',
      score:[
        {
          buk:0,
          jambas:0,
          jenis:0,
          taza:0
        },
        {
          buk:0,
          jambas:0,
          jenis:0,
          taza:0
        }
      ]
    };
  }

  componentWillReceiveProps(nextProps){
      if(nextProps.onePairExists ){
        nextProps.userAdmin.fetch().map((connection,index) => {
          if(connection.user==Meteor.user()._id){
            if(connection.idTour && connection.page && connection.idOnePair){
            this.setState({idTour:connection.idTour,idOnePair:connection.idOnePair,active:true})
            Meteor.call('findLastOnePairById',connection.idOnePair, (err,result) => {
              if (err) alert(err)
              else{
                this.setState({figters:result.data || [],idTour:result.idTour || '',elapsed:result.battleTimer || 300,isMedic:result.isMedic || false,beltfight:result.beltfight || false}) 
                this.setState({first_flag:'/flags/' + (countries[(this.state.figters[0].country).toUpperCase()]).toLowerCase() + '.png'})
                this.setState({second_flag:'/flags/' + (countries[(this.state.figters[1].country).toUpperCase()]).toLowerCase() + '.png'})
                if(this.state.isMedic.active){
                    let time = this.state.elapsed
                    this.props.history.push({pathname:'/user-medic',data:this.state.isMedic.fighter,time:time,callback:this.state.figters,id:this.state.idTour,idOnePair:this.state.idOnePair})      
                    
                }
                if(this.state.beltfight.active){
                    let time = this.state.elapsed
                    this.stopTimer()
                    this.props.history.push({pathname:'/user-beltfight',data:this.state.figters,time:time,callback:this.state.figters,id:this.state.idTour,idOnePair:this.state.idOnePair})   
                }
              }
            })
          }
          else {
            this.setState({active:false})
          }
        }}
      )}  
  }

  componentWillMount() {
    if(this.props.location.time){
    this.setState({figters:this.props.location.data,idTour:this.props.location.id,active:true,elapsed:this.props.location.time-Date.now()})
    }
  }

  tick = () => {
    Meteor.call('updateOnePair',this.state.idTour,this.state.figters,this.state.end - Date.now(),false,false,(err,result) => {
      if (err) alert(err)
      else{}     
    })
    if(this.state.elapsed<=0){
      clearInterval(this.timer);
      this.setState({isBattleEnd:true})
    }
  }

  stopTimer = () => { 
    this.setState({end:this.state.elapsed,isBattleContinue:false})
    clearInterval(this.timer);
  }
  
  startTimer = (seconds) =>{
    this.setState({isBattleStar:true,isBattleContinue:true})//Disable button "START BATTLE"
    let end = new Date()
    end.setMinutes(end.getMinutes()+5 )
    this.setState({end:end})
    this.timer = setInterval(this.tick, 50);

  }

  continueTimer = () =>{
    let end = Date.now()
    end = end +this.state.end
    this.setState({end:end,isBattleContinue:true})
    this.timer = setInterval(this.tick, 50);
  }

  callMedic = (fighter) => {
    let time = this.state.elapsed
    this.stopTimer()
    const medic = {active:true,fighter:fighter}
    Meteor.call('updateOnePair',this.state.idTour,this.state.figters,this.state.elapsed,medic,false,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }

  beltFight = () => {
    let time = this.state.elapsed
    this.stopTimer()
    const beltfight = {active:true,fighter:this.state.figters}
    Meteor.call('updateOnePair',this.state.idTour,this.state.figters,this.state.end - Date.now(),false,beltfight,(err,result) => {
      if (err) alert(err)
      else{}     
    })    
  }

  endBattle = () => {
    this.props.history.push({pathname:'/tour'})       
  }

  winner = (fighter) => {
    this.setState({clearWin:true})
    this.stopTimer()
    let id = 0
    let level = parseInt(fighter.id.charAt(0)) + 1
    parseInt(fighter.id.substr(2))%2==0
      ?id = parseInt(fighter.id.substr(2)) / 2
      :id = Math.floor(parseFloat(fighter.id.substr(2)) / 2)
      alert('WINNER: ' + fighter.name + ' ' + fighter.surname)
      this.setState({isBattleEnd:true})
      this.state.pairs.data[level][id] = {...fighter,id:level + '.' + id}
      Meteor.call('updatePairsById',this.state.idTour,this.state.pairs,(err,result) => {
        if (err) alert(err)
        else{
          console.log(result)
        }     
      })
      Meteor.call('updateLastHistoryTours',this.state.idTour,this.state.pairs,(err,result) => {
        if (err) alert(err)
        else{
          console.log(result)
        }     
      })
      Meteor.call('getLastHistoryTours',this.state.idTour,this.state.pairs,(err,result) => {
        if (err) alert(err)
        else{
          console.log(result)
        }     
      })
      this.setState({isBattleEnd:true})
      
  }

  scoring = (typeScore,fighter,index) => {
    const newState = this.state.score
    let level = parseInt(fighter.id.charAt(0))
    let id = parseInt(fighter.id.substr(2))
    fighter[typeScore]
      ?fighter[typeScore] += 1
      :fighter[typeScore] = 1
      console.log(fighter)
    switch(typeScore){
    case 0: {
      if(fighter[typeScore]%3==0) this.scoring(1,fighter,index)
        newState[index].buk = fighter[typeScore]
        console.log('0')
        break;
    }
    case 1: {
      if(fighter[typeScore]%2==0) this.scoring(2,fighter,index)
        newState[index].jambas = fighter[typeScore]
        console.log('1')
      break;
    }
    case 2: {
      if(fighter[typeScore]%2==0) this.scoring(3,fighter,index)
        newState[index].jenis = fighter[typeScore]
        console.log('2')
      break;
    }
    case 3: {
      newState[index].taza = fighter[typeScore]
      this.winner(fighter)
      console.log('3')
      break;
    }
    }
    this.setState({score:newState})
    const newStatePairs = this.state.pairs
    newStatePairs.data[level][id] = {...fighter}
    const newStateOnePair = this.state.figters
    newStateOnePair[index] = {...fighter}
    Meteor.call('changeFighters',this.state.idOnePair,newStateOnePair,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    this.setState({pairs:newStatePairs}) 
    console.log(this.state.pairs)
    Meteor.call('updatePairsById',this.state.idTour,this.state.pairs,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    Meteor.call('updateLastHistoryTours',this.state.idTour,this.state.pairs,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
    Meteor.call('getLastHistoryTours',this.state.idTour,this.state.pairs,(err,result) => {
      if (err) alert(err)
      else{
        console.log(result)
      }     
    })
  }


  render(){
    var elapsed = Math.round(this.state.elapsed / 100);
    var seconds = (elapsed / 10).toFixed(1) ; 
    var minutes = Math.ceil(parseInt( seconds / 60 ))
    seconds = Math.round(+(seconds))
    console.log(this.state.first_flag)
    const firstInputCountry = this.state.figters[0].country
    const secondInputCountry = this.state.figters[1].country
    const countryKeys = Object.keys(countries)
    const firstFindCountryMatch = countryKeys.find(country => country.indexOf((firstInputCountry).toUpperCase()) != -1)
    const secondFindCountryMatch = countryKeys.find(country => country.indexOf((secondInputCountry).toUpperCase()) != -1)
    const firstCountryFound = countries[firstFindCountryMatch]
    const secondCountryFound = countries[secondFindCountryMatch]
    // fighters cells class
    let f03Class = 'cell firstFighter number'
    let f02Class = 'cell firstFighter number'
    let f01Class = 'cell firstFighter number'
    let f00Class = 'cell firstFighter number'
    let f13Class = 'cell secondFighter number'
    let f12Class = 'cell secondFighter number'
    let f11Class = 'cell secondFighter number'
    let f10Class = 'cell secondFighter number'
    if (this.state.figters[0].warning3) f03Class += ' warning'
    if (this.state.figters[0].warning2) f02Class += ' warning'
    if (this.state.figters[0].warning1) f01Class += ' warning'
    if (this.state.figters[0].warning0) f00Class += ' warning'
    if (this.state.figters[1].warning3) f13Class += ' warning'
    if (this.state.figters[1].warning2) f12Class += ' warning'
    if (this.state.figters[1].warning1) f11Class += ' warning'
    if (this.state.figters[1].warning0) f10Class += ' warning'
    return(
      <div>
        { 
          this.state.active
            ? <div className="container-custom">  
                <div className="content">
                  <div className="indexTopBox">
                    <div className="tableRow">
                      <div className="cellLeft">
                        {
                          seconds<=0
                            ? <p>0:00 ({this.state.figters[0].weightCategory})</p>
                            : <p>{minutes}:{seconds % 60>9 ?seconds % 60 :"0"+seconds%60} ({this.state.figters[0].weightCategory})</p>
                        }
                      </div>
                      <div className="cellCenter">
                        <img width="70%" src="logo.png" />
                      </div>
                      <div className="cellRight">
                        {this.state.figters[0].weightCategory}
                      </div>
                    </div>
                  </div>
                  <div className="indexBottomBox">
                    <div className="tableRow">
                      <div className="cell firstFighter" style={{width: '17%'}}>
                        <img src={'/flags/' + firstCountryFound.toLowerCase() + '.png'} width="100%" alt="флаг россии"  style={{marginBottom: -5}}/>
                      </div>
                      <div className="cell firstFighter text" style={{width: '40%'}}>
                        <p><span>{firstCountryFound}</span> {this.state.figters[0].surname + ' ' + this.state.figters[0].name.charAt(0) + '. '}</p>
                      </div>
                      <div className={f03Class} style={{width: '5%'}}>{this.state.figters[0][3] || 0}</div>
                      <div className={f02Class} style={{width: '5%'}}>{this.state.figters[0][2] || 0}</div>
                      <div className={f01Class} style={{width: '5%'}}>{this.state.figters[0][1] || 0}</div>
                      <div className={f00Class} style={{width: '5%'}}>{this.state.figters[0][0] || 0}</div>
                    </div>
                    <div className="tableRow">
                      <div className="cellTwo"></div>
                      <div className="cellTwo"></div>
                      <div className="cellTwo">Т</div>
                      <div className="cellTwo">ЖЖ</div>
                      <div className="cellTwo">Ж</div>
                      <div className="cellTwo">Б</div>				
                    </div>
                    <div className="tableRow">
                      <div className="cell secondFighter">
                        <img src={'/flags/' + secondCountryFound.toLowerCase() + '.png'} width="100%" alt="флаг россии"  style={{marginBottom: -5}}/>                
                      </div>
                      <div className="cell secondFighter text">
                        <p><span>{secondCountryFound}</span>  {this.state.figters[1].surname + ' ' + this.state.figters[1].name.charAt(0) + '. '}</p>
                      </div>
                      <div className={f13Class}>{this.state.figters[1][3] || 0}</div>
                      <div className={f12Class}>{this.state.figters[1][2] || 0}</div>
                      <div className={f11Class}>{this.state.figters[1][1] || 0}</div>
                      <div className={f10Class}>{this.state.figters[1][0] || 0}</div>				
                    </div>
                  </div>
                </div>
              </div>
              :  <div className="splash">
                  <div className="content">
                    <div className="splashTopBox">
                      <div className="tableRow">
                        <div className="cell">
                          <img width="90%" src="logoworld.png"/>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              }
        <script src="modernizr-3.5.0.min.js"></script>
      </div>
    )
  }
})