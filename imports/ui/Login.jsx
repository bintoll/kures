import React,{Component} from 'react'
import { BrowserRouter as Router, Route, Link, Redirect, withRouter, withHistory } from 'react-router-dom'
import { Meteor } from 'meteor/meteor'
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';

export default class Login extends Component {
	constructor (props) {
		super(props);
		this.state = {
      form: {
        username: '',
        password: '',
      },
			error: null,
    };
  	}
  
  logIn = () => {
    const user = this.state.form.username
    const password = this.state.form.password
    Meteor.loginWithPassword(user, password, (_error, _result) => {
			if (_error) {
				switch (_error.reason) {
					case 'User not found':
						_errorMessage = 'Пользователь не найден !';
						break;
					case 'Incorrect password':
						_errorMessage = 'Вы ввели не верный пароль !';
						break;
					default:
						_errorMessage = _error.reason;
				}
				this.setState({error: _errorMessage});
      }
      else{
        this.props.history.push({pathname:'/admin', state:'lol'})
      }})
  }
  
  
  logOut = () => {
    Meteor.logout()
    setTimeout(()=>this.props.history.push({pathname:'/'}),500)
  }
  
  
  onChangeText = (e,fieldName) => {
    newState = this.state.form
    newState[fieldName] = e.target.value
    this.setState({form: newState})
  }

	render() {
		return (
			<div className="container">
				<div className="row">
					<div className="col-md-4 col-md-offset-4">
						<div className="login-panel panel panel-default">
							<div className="panel-heading">
								<h3 className="panel-title">Авторизация</h3>
							</div>
							<div className="panel-body">
                {this.state.error ? (<p className="text-center">{this.state.error}</p>) : null}
								<form role="form" onSubmit={this.handleSubmit}>
									<fieldset>
										<div className="form-group">
											<input
												className="form-control"
												placeholder="E-mail или логин"
												type="text"
												autoFocus
												onChange={(e)=>this.onChangeText(e,'username')}
												value={this.state.username} />
										</div>
										<div className="form-group">
											<input
												className="form-control"
												placeholder="******"
												type="password"
												onChange={(e)=>this.onChangeText(e,'password')}
												value={this.state.password} />
										</div>
                    <input type="button" onClick={()=>this.logIn()} value="Войти" className="btn btn-lg btn-success btn-block"/>
                  </fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		);
	}
}