import React,{Component} from "react";
import { BrowserRouter as Router, Route, Redirect, withRouter, withHistory} from 'react-router-dom'
import {createContainer} from "meteor/react-meteor-data";
import TopBar from './TopBar'
import HeaderTitle from "../client/Index/HeaderTitle";
import * as constants from "../client/Index/constants";
import { Button, Col, ControlLabel, DropdownButton, Form, FormControl, FormGroup, MenuItem, Pagination, Panel, Row, Modal} from "react-bootstrap";

import { withTracker } from 'meteor/react-meteor-data';
import ReactDOM from 'react-dom';
import { Accounts } from 'meteor/accounts-base';
import { Mongo } from 'meteor/mongo';

export default AdminPanelContainer = withTracker(() => {
  
    const users = Meteor.subscribe('user');
    const loading = !users.ready();
    return {
      users,
      loading,
    };
  })(
class AdminPanel extends Component {
	constructor (props) {
		super(props);
		this.state = {
			showDeleteModal: false,
			name: '',
      role: '',
      allAccounts:[],
      form: {
        username: '',
        password: '',
        createUser: ''
      },
      user:{},
		};
	}

  componentWillReceiveProps(nextProps){
    console.log(nextProps)
  }
  
  componentWillMount(){
    Meteor.call('getUser',(err,result)=>{
      if(err) console.log(err)
        else this.setState({user:result})
    } )
    this.getAllAccounts()//get all accs in state.allAccounts
}

logOut = () => {//TOP BAR Method
  Meteor.logout()
  setTimeout(()=>this.props.history.push({pathname:'/'}),500)
}

  createUser = () => {//insert user in db
    const user = this.state.form.createUser
    const password = this.state.form.password
    Meteor.call('createAccount',user,password, (error, result) => {
      if(error) alert(error)
      else      this.getAllAccounts()})
      this.setState({showDeleteModal:false})
}

  getAllAccounts = () => {//get all accs in state.allAccounts
    Meteor.call('getAllAccounts', (error, result) => {
      if(error)  console.log(error)
      else       this.setState({allAccounts: result})})
}

filterAccs = (role) => {//filer accounts in table by role
  this.setState({role:role})
  if(role=='admin')
    {
      let allAccounts = []
      this.state.allAccounts.map((user)=>{
        Roles.userIsInRole(user,'admin')
        ? allAccounts.push(user)
        : null
      })
      this.setState({allAccounts:allAccounts})
    }
    else {
     this.getAllAccounts()
     
    }
}

findAcc = (username) => {//find acc by substring inserted in field
  let len = username.length
  let allAccounts = []
  this.setState({name:username})
  this.state.allAccounts.map((user)=>{
    user.username.substring(0,len) == username
    ? allAccounts.push(user)
    : null
  })
  this.setState({allAccounts:allAccounts})
  if(username=='')
    this.getAllAccounts()
}

  changeRole = (addOrRemove,username,role) => {//remove or delete role on user, addOrRemove = true if add role
    let userToRole
    this.getAllAccounts()
    this.state.allAccounts.map((user,index) => {
      user.username == username
        ? userToRole = user
        :null})
    addOrRemove
      ? Meteor.call('addRoleToUser', userToRole, role, (error, result) => {
          if(error)  alert(error)
          else       this.getAllAccounts()})
      : Meteor.call('removeRoleFromUser', userToRole, role, (error, result) => {
        if(error)  alert(error)
          else      this.getAllAccounts()})
}

  onChangeText = (e,fieldName) => {
    newState = this.state.form
    newState[fieldName] = e.target.value
    this.setState({form: newState})
}

deleteAccount = (username) => {
  let userTodelete
  this.getAllAccounts()
  this.state.allAccounts.map((user,index) => {
    user.username == username
      ? userToRole = user
      :null })
    Meteor.call('deleteAccount', userToRole._id,(error, result) => {
      if(error) alert(error)
      else      this.getAllAccounts()})
}

goTo = (path) => {//TOP BAR Method
  this.props.history.push(path)
}

connectUser = (id) => {
  console.log(id)
  Meteor.call('connectUser', id,(error, result) => {
    if(error) alert(error)
    else      console.log(result)
    })
    Meteor.call('getAllConnections',(error, result) => {
      if(error) alert(error)
      else      console.log(result)
      })
}


	render () {
		return (
			<div>
        <TopBar currentUser={this.state.user} goTo={(path)=>this.goTo(path)} logOut={()=>this.logOut()}/>
				<Modal show={this.state.showDeleteModal} onHide={() => this.setState({userId: null, showDeleteModal: false})}>
					<Modal.Header closeButton>
						<Modal.Title>Создание новой учетной записи</Modal.Title>
					</Modal.Header>
					<Modal.Body>
          <form role="form">
					<fieldset>
						<div className="form-group">
							<input
								className="form-control"
								placeholder="Логин"
								type="text"
								autoFocus
                onChange={(e)=>this.onChangeText(e,'createUser')}
                value={this.state.username} />
						</div>
						<div className="form-group">
							<input
								className="form-control"
			  				placeholder="******"
		  					type="password"
								onChange={(e)=>this.onChangeText(e,'password')}
								value={this.state.password} />
						</div>
            </fieldset>
            </form>
					</Modal.Body>
					<Modal.Footer>
						<Button onClick={() => this.setState({userId: null, showDeleteModal: false})}>Отмена</Button>
						<Button onClick={()=>this.createUser()} bsStyle="success">Создать</Button>
					</Modal.Footer>
				</Modal>
        <div style={{marginLeft:260}}>
				<HeaderTitle title="Пользователи"/>
				<Row>
					<Col xs={12}>
						<Panel header="База данных пользователей">
							<Form style={{marginBottom: '10px',textAlign:'center'}} inline>
								<FormGroup controlId="formInlineName">
									<ControlLabel>Роль:&nbsp;&nbsp;</ControlLabel>
									<FormControl componentClass="select" placeholder="Роли" bsSize="small" value={this.state.role} onChange={(e) => this.filterAccs(e.target.value)}>
										<option value="">-</option>
										<option value="admin">Администратор</option>
									</FormControl>
								</FormGroup>
								&nbsp;&nbsp;
								<FormGroup controlId="formInlineEmail">
									<ControlLabel>Имя:&nbsp;&nbsp;</ControlLabel>
									<FormControl placeholder="логин" bsSize="small" value={this.state.name} onChange={(e) => this.findAcc(e.target.value)}/>
								</FormGroup>
								&nbsp;&nbsp;
								<Button type="button" bsStyle="default" bsSize="sm" onClick={() => this.setState({showDeleteModal:true})}>Добавить
									пользователя</Button>
							</Form>
							<Row>
								<Col xs={12}>
									<table width="100%" className="table table-striped table-bordered table-hover">
										<thead>
										<tr>
											<th>Имя</th>
											<th width="1%" className="text-center">Роль</th>
											<th width="1%"></th>
										</tr>
										</thead>
										<tbody>
										{
											this.state.allAccounts.map((_user) => {
												return (
													<tr key={'row-' + _user._id}>
														<td>{_user.username}</td>
														{
                        Roles.userIsInRole(_user,['admin'])
                          ? <td>admin</td>
                          : Roles.userIsInRole(_user,'judje')
                            ?<td>judje</td>
                            :<td>simple</td>
                            }
														<td className="text-center">
															<FormGroup>
																<DropdownButton
																	id={'dropdown-users-' + _user._id}
																	bsSize="sm"
																	bsStyle="link"
																	title="Действие"
																	pullRight
																	disabled={false}>
																	<MenuItem header>Выберите действие</MenuItem>
																	<MenuItem divider />
																	<MenuItem eventKey="1" onClick={()=>this.deleteAccount(_user.username)} >Удалить</MenuItem>
																	<MenuItem eventKey="2" onClick={()=>this.changeRole(false,_user.username,'admin')} >Убрать роль</MenuItem>
                                  <MenuItem eventKey="2" onClick={()=>this.changeRole(true, _user.username,['admin','judje'])}>Назначить роль админа</MenuItem>
                                  <MenuItem eventKey="2" onClick={()=>this.changeRole(false, _user.username,'judje')}>Убрать роль судьи</MenuItem>
                                  <MenuItem eventKey="2" onClick={()=>this.changeRole(true, _user.username,'judje')}>Назначить роль судьи</MenuItem>
																</DropdownButton>
															</FormGroup>
														</td>
													</tr>
												);
											})
										}
										</tbody>
									</table>
								</Col>
							</Row>
						</Panel>
					</Col>
				</Row>
        </div>
      </div>
		);
	}
}
)
