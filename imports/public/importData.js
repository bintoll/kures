import {Mongo} from "meteor/mongo";
const importData = new Mongo.Collection('importData');
importData.before.insert(function (userId, doc) {
  //const count = importData.find({}).count() + 1;
  doc.createdAt = new Date();
  doc.updatedAt = '';
  doc.used = false;
  //doc.autoIncrement = count;
});

export default importData;