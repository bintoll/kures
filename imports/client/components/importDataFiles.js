import {FilesCollection} from 'meteor/ostrio:files';

var fs = Npm.require('fs');
var path = Npm.require('path');
var basepath = path.resolve('.').split('.meteor')[0];

const importDataFiles = new FilesCollection({
	collectionName: 'importDataFiles',
	allowClientCode: true,
	debug: true,
	onBeforeUpload: function (file) {
		if (file.size <= 50485760 && /xls|xlsx|csv/i.test(file.extension)) {
			return true;
		} else {
			return 'Please upload image, with size equal or less than 50MB';
		}
	},	
});

export default importDataFiles;
