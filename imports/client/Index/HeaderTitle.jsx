import React from "react";
const HeaderTitle = (props) => {
	return (
		<div className="row">
			<div className="col-lg-12">
				<h1 className="page-header">{props.title}</h1>
			</div>
		</div>
	);
};
HeaderTitle.defaultProps = {
	title: 'Title'
};
export default HeaderTitle;

