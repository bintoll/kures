import {FilesCollection} from 'meteor/ostrio:files';

const importDataFiles = new FilesCollection({
	collectionName: 'importDataFiles',
	allowClientCode: true,
	debug: true,
	onBeforeUpload: function (file) {
		if (file.size <= 50485760 && /xls|xlsx|csv/i.test(file.extension)) {
			return true;
		} else {
			return 'Please upload image, with size equal or less than 50MB';
		}
	},	
});

export default importDataFiles;
