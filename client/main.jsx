import React from 'react';
import { Meteor } from 'meteor/meteor';
import { render } from 'react-dom';
 

import PageRouterContainer from '../imports/ui/PageRouter.jsx'
import Fight from '../imports/ui/Fight.jsx'
import BeltFight from '../imports/ui/BeltFight.jsx'
import FightersTable from '../imports/ui/FightersTable.jsx'
import '/client/styles.less' 

Meteor.startup(() => {
  render(< PageRouterContainer/>, document.getElementById('render-target'));
});