import { Accounts } from 'meteor/accounts-base';
import { Meteor } from 'meteor/meteor';

import {FilesCollection} from 'meteor/ostrio:files';
import importDataFiles from '/imports/public/importDataFiles.js'
import { Mongo } from 'meteor/mongo';


var fs = Npm.require('fs');
var path = Npm.require('path');
var basepath = path.resolve('.').split('.meteor')[0];
var tmp_idtour, tmp_end, tmp_stop,tmp_idOnePair,tmp_state
var Fiber = require('fibers');
var timers = []
var tmp_fighters = []



if (Meteor.isServer) {
  Meteor.publish('uploadsFiles', function (_QrCodeSession) {
    if (this.userId) {
      return importDataFiles.find({}).cursor;
    } else {
      this.stop();
    }
  });
}


tick = (idTour,fighters,id,end,state) =>{
  tmp_fighters.map((fighter,index) => {
        if(id==fighter.id)
          {
            Fiber(function(){
              Meteor.call('updateOnePairById',id,idTour,fighter.fighters,end-Date.now(),false,false,state,(err,result) =>{
              });
            }).run();

          }
      })
}

medicTick = (idTour,fighters,id,end,state,medicEnd) =>{
  tmp_fighters.map((fighter,index) => {
        if(id==fighter.id)
          {
            Fiber(function(){
              Meteor.call('updateOnePairById',id,idTour,fighter.fighters,end,true,false,state,false,medicEnd,(err,result) =>{
              });
            }).run();

          }
      })
}

Meteor.methods({
    'createAccount': function(username,password){
      return Accounts.createUser({username: username, password: password})
    },
    'getAllAccounts': function(){
      return Meteor.users.find().fetch()
    },
    'getAllConnections': function(){
      return UserAdmin.find().fetch()
    },
    'connectUser': (id,tourId) => {
      return UserAdmin.update({
        user:id
      },{
      idTour:tourId,
      user:id,
      page:false,
      },{
        upsert:true
      });
    },
    'insertFighters': (data) => {
      return Fighters.insert({
      data:data,
      createdAt: new Date(), // current time
      });
    },
    'insertFighter': (id,surname,name,country,weightCategory) => {
      return FightersBase.insert({
        idProtocol:id,
        name:name,
        surname:surname,
        country:country,
        weightCategory:weightCategory
      });
    },
    'insertPairsTour': (data) => {
      return PairsTour.insert({
      data:data,
      createdAt: new Date(), // current time
      });
    },
    'insertOnePair': (data,idTour,state) => {
      return OnePair.insert({
      data:data,
      idTour:idTour,
      state:state,
      createdAt: new Date(), // current time
      });
    },
    'updateFightersBase': (id,name,surname,country,weight) => {
      return FightersBase.update({
        idProtocol:id,
        name:name,
        surname:surname,
        country:country,
        weightCategory:weight
      },
      {
        idProtocol:id,
        name:name,
        surname:surname,
        country:country,
        weightCategory:weight
      },
      {upsert:true}
    );
    },
    'updateFightersBaseById': (id,idProtocol,name,surname,country,weight) => {
      return FightersBase.update({
        _id:id
      },
      {
        idProtocol:idProtocol,
        name:name,
        surname:surname,
        country:country,
        weightCategory:weight
      },
    );
  },
  'updateUserAdmin': (userId,page,data,idTour,idOnePair) => {
      return UserAdmin.update({
        user:userId
      },
      {
        user:userId,
        page:page,
        data:data,
        idTour:idTour,
        idOnePair:idOnePair
      },
    );
  },
  'updatePairsById': (id,data) => {
      return Pairs.update({
        _id:id
      },
      {
        data:data.data,
        amountLevels:data.amountLevels,
        createdAt: new Date() // current time
      },
        );
  },
  'updateLastHistoryTours': (idPair,data) => {
    return HistoryTours.update({
        _id:HistoryTours.findOne({},{sort:{createdAt: -1, limit: -1}})._id
      },
      {
        id:idPair,
        data:data.data,
        amountLevels:data.amountLevels,
        createdAt: new Date()  // current time
      },
        );
  },
    'updateOnePair': (idTour,data,battleTimer,isMedic,beltfight) => {
    return OnePair.update({
        _id:OnePair.findOne({},{sort:{createdAt: -1, limit: -1}})._id
      },
      {
        idTour:idTour,
        battleTimer:battleTimer ?battleTimer :tmp_end-Date.now(),
        data:data,
        isMedic:isMedic,
        beltfight:beltfight,
        createdAt: new Date()  // current time
      },
        );
  },
  'updateOnePairById': (id,idTour,data,battleTimer,isMedic,beltfight,state,winnerID,medicTimer) => {
    timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer={...timer,fighters:data}
          }
        })
    return OnePair.update({
        _id:id
      },
      {
        idTour:idTour,
        battleTimer:battleTimer ?battleTimer :tmp_end-Date.now(),
        data:data,
        isMedic:isMedic,
        beltfight:beltfight,
        state:state,
        winnerID:winnerID ?winnerID :false,
        medicTimer:medicTimer,
        createdAt: new Date()  // current time
      },
        );
  },
    'insertPairs': (data,amountLevels) => {
      return Pairs.insert({
      data:data,
      amountLevels:amountLevels ?amountLevels :new Date(),
      createdAt: new Date(), // current time
      });
    },
    'findPairsById': (id) => {
      return Pairs.findOne({_id:id})
    },
    'findLastHistoryTours': () => {
      return HistoryTours.findOne({},{sort:{createdAt: -1, limit: -1}});
    },
    'findConnection': (adminId) => {
      return UserAdmin.findOne({},{admin:adminId})
    },
    'findConnections': (tourId) => {
      return UserAdmin.find().fetch()
    },
    'getLastHistoryTours': () => {
      return HistoryTours.find().fetch()
    },
    'insertHistoryTours': (data,amountLevels) => {
      return HistoryTours.insert({
      data:data,
      amountLevels:amountLevels ?amountLevels :new Date(),
      createdAt: new Date(), // current time
      });
    },
    'findLastPair': () => {
      return Pairs.findOne({},{sort:{createdAt: -1, limit: -1}});
    },
    'findLastPairTour': () => {
      return PairsTour.findOne({},{sort:{createdAt: -1, limit: -1}});
    },
    'findLastOnePair': () => {
      return OnePair.findOne({},{sort:{createdAt: -1, limit: -1}});
    },
    'findLastOnePairById': (id) => {
      return OnePair.findOne({_id:id})
    },
    'findLastpairs': () => {
      return OnePair.find().fetch()
    },
    'removeFromBase': (id) => {
      return FightersBase.remove({
      _id:id,
      });
    },
    'removeOnePair': (id) => {
      return OnePair.remove({
      _id:id,
      });
    },
    'removeFromUseradmin': (id) => {
      return UserAdmin.remove({
      _id:id,
      });
    },
    'getFighters': function(){
      return Fighters.find().fetch()
    },
    'getFightersBase': function(){
      return FightersBase.find().fetch()
    },
    'getPairs': function(){
      return Pairs.find().fetch()
    },
    'addRoleToUser': function(user, role){
      return Roles.addUsersToRoles(user, role, Roles.GLOBAL_GROUP);
    },
    'removeRoleFromUser': function(user, role){
      return Roles.removeUsersFromRoles(user, role, Roles.GLOBAL_GROUP);
    },
    'deleteAccount': function(userId){
      return Meteor.users.remove(userId);
    },
    'isUserInRole': () => {
      return Roles.userIsInRole(Meteor.user(),'admin')
    },
    'isUsersInRole': (user) => {
      return Roles.userIsInRole(user,'admin')
    },
    'getUser': () => {
      return Meteor.user()
    },
    'excelToArray': (fileId) => {
      const excel = new Excel('xlsx');
      console.log('importDataFiless')
      const fileFound = importDataFiles.findOne({ _id: fileId })
      console.log(fileFound)
      if (fileId) {
        const workbook = excel.readFile(fileId)
        const worksheet = workbook.SheetNames[0]
        var sheet = workbook.Sheets[worksheet]
        var options = { header : ['idProtocol', 'surname', 'name' ,'country','weightCategory' ] }
        var workbookJson = excel.utils.sheet_to_json( sheet, options );
        return workbookJson 
      }
      return console.log({status: 'failed', detail: 'Error finding file in database by id'})
    },
    'startTimer': (idTour,fighters,id,state) => {
      let end = new Date().setSeconds(new Date().getSeconds()+5 )
      tmp_fighters.push({id:id,fighters:fighters})
      tmp_fighters.map((fighter,index) => {
        if(id==fighter.id)
          {
            timers.push({timer:setInterval(this.tick,50,idTour,fighter.fighters,id,end,state),id:id,idTour:idTour,fighters:fighters,idOnePair:id,state:state,end:end})

          }
      })
    },
    'startMedicTimer': (id,fighterId) => {
      let medicEnd = new Date().setMinutes(new Date().getMinutes()+3 )
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.fighters.map((fighter,index) => {
              if(fighter._id==fighterId)
                {
                  fighter.medicEnd = medicEnd
                  fighter.medicTimer = setInterval(this.medicTick,50,timer.idTour,timer.fighters,timer.id,timer.end,timer.state,medicEnd)
                }
            })
          }
      })
    },
    'stopTimer': (id,state) => {
      tmp_state=state
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            if(timer.state=='Active')
              {
            timer.stop = timer.end-Date.now()
              }
              timer.state = 'Active-Paused'
            clearInterval(timer.timer);
            Fiber(function(){
              Meteor.call('updateOnePairById',timer.idOnePair,timer.idTour,timer.fighters,timer.stop,false,false,'Active-Paused',false,(err,result) =>{
              });
            }).run();

          }
      })
      tmp_stop = tmp_end - Date.now()
    },
    'callMedic': (id,medic,fighterId) => {
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            if(timer.state=='Active'){
              timer.stop = timer.end-Date.now()
              clearInterval(timer.timer);
 
            }
              timer.state=='Active-Paused'
              timer.fighters.map((fighter,index) => {
              if(fighter._id==fighterId)
                {
                  Fiber(function(){
                    Meteor.call('updateOnePairById',timer.idOnePair,timer.idTour,timer.fighters,timer.stop,medic,false,'Active-Paused',false,fighter.medicStop,(err,result) =>{
                    });
                    }).run();
                }
            })

          }
      })
    },
    'endMedic': (id,medic,fighterId) => {
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.fighters.map((fighter,index) => {
              if(fighter._id==fighterId)
                {
                  fighter.medicStop = fighter.medicEnd-Date.now()
                  timer.state = 'Active-Paused'
                  clearInterval(fighter.medicTimer);
                  Fiber(function(){
                    Meteor.call('updateOnePairById',timer.idOnePair,timer.idTour,timer.fighters,timer.stop,medic,false,'Active-Paused',false,true,(err,result) =>{
                    });
                  }).run();
                }
            })
          }
      })
    },
    'continueMedicTimer': (id,fighterId) => {
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.fighters.map((fighter,index) => {
              if(fighter._id==fighterId)
                {
                  fighter.medicEnd = Date.now()+fighter.medicStop
                  fighter.medicTimer = setInterval(this.medicTick,50,timer.idTour,timer.fighters,timer.id,timer.end,timer.state,fighter.medicEnd)
                }
            })
          }
          
      })
    },
    'stopMedicTimer': (id,fighterId) => {
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.fighters.map((fighter,index) => {
              if(fighter._id==fighterId)
                {
                  clearInterval(fighter.medicTimer);
                }
            })
          }
          
      })
    },
    'stopTimerBeltFight': (id,state,beltfight) => {
      tmp_state=state
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            clearInterval(timer.timer);

          }
      })
      tmp_stop = tmp_end -Date.now()
      Fiber(function(){
        Meteor.call('updateOnePairById',tmp_idOnePair,tmp_idtour,tmp_fighters,tmp_stop,false,beltfight,state,(err,result) =>{
        });
      }).run();
    },
    'changeFighters': (id,fighters,winner) => {
      tmp_fighters.map((fighter,index) => {
        if(id==fighter.id)
          {
            fighter.fighters=fighters
          }
      })
      tmp_idOnePair=id
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.fighters=fighters
            if(timer.state=='Active'){
              timer.stop = timer.end-Date.now()
            }
            Fiber(function(){
        Meteor.call('updateOnePairById',id,timer.idTour,fighters,timer.stop,false,false,timer.state,(err,result) =>{
        });
      }).run();

          }
      })
    },
    'continueTimer': (id,fighters,state) => {
      tmp_state = state
      let end = Date.now()
      tmp_end = end + tmp_stop
      timers.map((timer,index) => {
        if(id==timer.id)
          {
            timer.end = Date.now()+timer.stop
            timer.state = 'Active'
            timer.timer = setInterval(this.tick,50,timer.idTour,fighters,timer.id,Date.now()+timer.stop,timer.state)
            timer={...timer,state:state,fighters:fighters}
          }
          
      })

    },
    uploadsDelete(_fileId){
			if (!this.userId) throw new Meteor.Error(403);
			if (!_fileId)  throw new Meteor.Error(404);
			const deleteAction = importDataFiles.remove({ _id: _fileId });
			if (deleteAction) {
				return true;
			} else {
				return false;
			}
		},
    uploadsParse(_fileId){
      return true;
    }

})

Meteor.publish('historyTours',() => {
    return HistoryTours.find({})
  });
  Meteor.publish('onePair',() => {
    return OnePair.find({})
  });
  Meteor.publish('user',() => {
    return Meteor.users.find({})
  });
  Meteor.publish('userAdmin',() => {
    return UserAdmin.find({})
  });
  Meteor.publish('pairs',() => {
    return Pairs.find({})
  });
  Meteor.publish('fightersBase',() => {
    return FightersBase.find({})
  });

Meteor.startup(()=> {
  if (Meteor.users.find().fetch().length==[])
    {
      const admin = Accounts.createUser({username: 'admin', password: '123'})
      Roles.addUsersToRoles(admin, 'admin', Roles.GLOBAL_GROUP);
    }
})